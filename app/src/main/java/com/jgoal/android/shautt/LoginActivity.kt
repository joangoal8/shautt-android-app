package com.jgoal.android.shautt

import android.content.DialogInterface
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AlertDialog
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.common.api.ApiException
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.GoogleAuthProvider
import com.jgoal.android.shautt.config.ShauttConfig
import com.jgoal.android.shautt.loginregister.LoginView
import com.jgoal.android.shautt.loginregister.ProviderLogin
import com.jgoal.android.shautt.loginregister.UserLoginRegisterController
import kotlinx.android.synthetic.main.activity_login.*
import javax.inject.Inject

class LoginActivity : AppCompatActivity(), LoginView {

    private val googleSignInCode = 8291
    
    @Inject lateinit var auth: FirebaseAuth
    @Inject lateinit var firebaseAnalytics: FirebaseAnalytics
    @Inject lateinit var userLoginRegisterController: UserLoginRegisterController

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        (application as ShauttApplication).shauttComponent.inject(this)

        val bundle = Bundle()
        firebaseAnalytics.logEvent(FirebaseAnalytics.Event.APP_OPEN, bundle)

        setup()
        userLoginRegisterController.setLoginView(this)
    }

    private fun setup() {
        val user = auth.currentUser
        if (user != null) {
            print(user)
            val uid = user.uid
            val email = user.email
            userLoginRegisterController.loginUser(uid, email!!, ProviderLogin.EMAIL_PWD)
            return
        }
        configureRegisterButton()
        configureLoginButtons()
    }

    private fun configureRegisterButton() {
        registerBtnInLogin.setOnClickListener {
            val registerIntent: Intent = Intent(this, RegisterActivity::class.java).apply {
                print("switching to REGISTER")
                Log.d("######### USER FIRE STORE ########", userLoginRegisterController.toString())
            }
            startActivity(registerIntent)
        }
    }

    private fun configureLoginButtons() {
        loginButtonInLogin.setOnClickListener {
            if (mailLoginInputTxt.text.isNotEmpty() && pwdLoginInputTxt.text.isNotEmpty()) {
                auth.signInWithEmailAndPassword(
                    mailLoginInputTxt.text.toString(),
                    pwdLoginInputTxt.text.toString()
                ).addOnCompleteListener {
                    if (it.isSuccessful) {
                        val user = it.result?.user
                        val uid = user?.uid
                        val email = user?.email
                        userLoginRegisterController.loginUser(uid!!, email!!, ProviderLogin.EMAIL_PWD)
                    } else {
                        showAlert()
                    }
                }
            }
        }
        googleSignInButtonInLogin.setOnClickListener {
            val googleConf = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN).requestIdToken(getString(R.string.default_web_client_id)).requestEmail().build()

            val googleClient = GoogleSignIn.getClient(this, googleConf)
            googleClient.signOut()

            startActivityForResult(googleClient.signInIntent, googleSignInCode)
        }
    }

    private fun showAlert() {
        val builder = AlertDialog.Builder(this)
        builder.setTitle("Error Login")
        builder.setMessage("Se ha producido un error autenticando al usuario")
        builder.setPositiveButton("Aceptar") { dialogInterface: DialogInterface, i: Int ->
            print("click aceptar $i")
        }
        builder.setNegativeButton("Cancelar") { dialogInterface: DialogInterface, i: Int ->
            print("click cancelar $i")
        }
        val dialog : AlertDialog = builder.create()
        dialog.show()
    }

    override fun switchToTabBar() {
        val tabBarIntent : Intent = Intent(this, TabBarActivity::class.java).apply {
            print("switching to REGISTER")
        }
        startActivity(tabBarIntent)
    }

    override fun displayError() {
        Log.d("ERROR", " HIIIII BIGG ERROR BUDDYYY")
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == googleSignInCode) {
            val task = GoogleSignIn.getSignedInAccountFromIntent(data)

            try {
                val account = task.getResult(ApiException::class.java)

                if (account != null) {
                    val credential = GoogleAuthProvider.getCredential(account.idToken, null)
                    auth.signInWithCredential(credential).addOnCompleteListener {
                        if (it.isSuccessful) {
                            val user = it.result?.user
                            val uid = user?.uid
                            val email = account.email
                            Intent(this, RegisterActivity::class.java).apply {
                                Log.d("######### USER FIRE STORE ########", userLoginRegisterController.toString())
                            }
                            userLoginRegisterController.loginUser(uid!!, email!!, ProviderLogin.GOOGLE)
                        } else {
                            showAlert()
                        }
                    }
                }
            } catch (e: ApiException) {
                showAlert()
            }
        }
    }

}