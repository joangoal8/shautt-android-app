package com.jgoal.android.shautt

import android.content.DialogInterface
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.appcompat.app.AlertDialog
import com.google.firebase.auth.FirebaseAuth
import com.jgoal.android.shautt.commons.ShauttSlackMessageService
import com.jgoal.android.shautt.loginregister.RegisterView
import com.jgoal.android.shautt.loginregister.UserLoginRegisterController
import com.jgoal.android.shautt.model.SlackChannel
import com.jgoal.android.shautt.onboarding.OnBoardingActivity
import kotlinx.android.synthetic.main.activity_register.*
import javax.inject.Inject

class RegisterActivity : AppCompatActivity(), RegisterView {

    @Inject lateinit var auth: FirebaseAuth
    @Inject lateinit var userLoginRegisterController: UserLoginRegisterController
    @Inject lateinit var shauttSlackMessageService: ShauttSlackMessageService

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register)

        (application as ShauttApplication).shauttComponent.inject(this)

        setup()
        userLoginRegisterController.setRegisterView(this)
    }

    private fun setup() {
        registerBtnRegister.setOnClickListener {
            if (mailRegisterInputTxt.text.isNotEmpty() && pwdRegisterInputTxt.text.isNotEmpty()) {
                auth.createUserWithEmailAndPassword(mailRegisterInputTxt.text.toString(),
                    pwdRegisterInputTxt.text.toString()).addOnCompleteListener {task ->
                    if (task.isSuccessful) {
                        print("USER REGISTERED")
                        val user = task.result?.user
                        val uid = user?.uid
                        val email = user?.email
                        val slackMessage = "New user is registered: $email \n in ANDROID"
                        shauttSlackMessageService.sendSlackMessage(SlackChannel.SEARCH, slackMessage)
                        userLoginRegisterController.configureUserWithShoppingBasket(uid!!, email!!)
                    } else {
                        showAlert()
                    }
                }
            }
        }
        backToLogin.setOnClickListener {
            val loginIntent : Intent = Intent(this, LoginActivity::class.java).apply {
                print("switching to Login")
            }
            startActivity(loginIntent)
        }
    }

    private fun showAlert() {
        val builder = AlertDialog.Builder(this)
        builder.setTitle("Error")
        builder.setMessage("Se ha producido un error autenticando al usuario")
        builder.setPositiveButton("Aceptar") { dialogInterface: DialogInterface, i: Int ->
            print("click aceptar $i")
        }
        builder.setNegativeButton("Cancelar") { dialogInterface: DialogInterface, i: Int ->
            print("click cancelar $i")
        }
        val dialog : AlertDialog = builder.create()
        dialog.show()
    }

    override fun isOKAndGoToTabBar() {
        goToOnBoarding()
    }

    private fun goToOnBoarding() {
        val onBoardingIntent : Intent = Intent(this, OnBoardingActivity::class.java).apply {
            print("switching to On Boarding")
        }
        startActivity(onBoardingIntent)
    }

    override fun showError() {
        showAlert()
    }
}