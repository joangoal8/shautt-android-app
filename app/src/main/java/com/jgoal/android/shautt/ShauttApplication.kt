package com.jgoal.android.shautt

import android.app.Application
import com.jgoal.android.shautt.component.DaggerShauttComponent
import com.jgoal.android.shautt.component.ShauttComponent
import com.jgoal.android.shautt.modules.ShauttModule

class ShauttApplication : Application() {

    lateinit var shauttComponent: ShauttComponent

    override fun onCreate() {
        super.onCreate()
        shauttComponent = initDagger()
    }

    private fun initDagger() = DaggerShauttComponent.builder().shauttModule(ShauttModule(this)).build()
}