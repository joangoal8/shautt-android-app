package com.jgoal.android.shautt

import android.content.DialogInterface
import android.content.Intent
import android.graphics.BitmapFactory
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.EditText
import android.widget.Button
import android.widget.ImageButton
import android.widget.ImageView
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.libraries.places.api.Places
import com.google.android.material.textfield.TextInputLayout
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.storage.FirebaseStorage
import com.jgoal.android.shautt.adapters.EditableShoppingCartProductCellAdapter
import com.jgoal.android.shautt.commons.FirebaseMetricsName
import com.jgoal.android.shautt.fragment.ProfileFragment
import com.jgoal.android.shautt.fragment.ScannerFragment
import com.jgoal.android.shautt.fragment.ShoppingListFragment
import com.jgoal.android.shautt.loginregister.UserLoginRegisterController
import com.jgoal.android.shautt.model.MasterCartDto
import com.jgoal.android.shautt.model.ProductTypeDto
import com.jgoal.android.shautt.model.ShoppingCartCandidateDto
import com.jgoal.android.shautt.model.SlackChannel
import com.jgoal.android.shautt.shoppingcart.*
import com.jgoal.android.shautt.shoppinglist.ProductsFragment
import com.jgoal.android.shautt.shoppinglist.ShoppingListController
import com.jgoal.android.shautt.userprofile.MyOrdersFragment
import com.jgoal.android.shautt.userprofile.UserProfileController
import com.jgoal.android.shautt.commons.ShauttSlackMessageService
import kotlinx.android.synthetic.main.activity_tab_bar.*
import java.util.*

import javax.inject.Inject

class TabBarActivity : AppCompatActivity() {

    @Inject
    lateinit var shoppingListController: ShoppingListController
    @Inject lateinit var shoppingCartController: ShoppingCartController
    @Inject lateinit var firebaseStorage: FirebaseStorage
    @Inject lateinit var userProfileController: UserProfileController
    @Inject lateinit var userLoginRegisterController: UserLoginRegisterController
    @Inject lateinit var shauttSlackMessageService: ShauttSlackMessageService

    private var shoppingList = Collections.emptyList<ProductTypeDto>()
    private lateinit var postcode: String

    private lateinit var firebaseAnalytics: FirebaseAnalytics

    var shoppingCarts: List<ShoppingCartCandidateDto> = Collections.emptyList()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_tab_bar)

        (application as ShauttApplication).shauttComponent.inject(this)

        firebaseAnalytics = FirebaseAnalytics.getInstance(this)

        tabBarConfiguration()
    }

    private fun tabBarConfiguration() {
        val shoppingListFragment = ShoppingListFragment()
        val scannerFragment = ScannerFragment()
        val profileFragment = ProfileFragment()

        switchSelectedFragment(shoppingListFragment)

        tab_bar_navigation.setOnNavigationItemSelectedListener {
            when (it.itemId) {
                R.id.ic_shopping_list -> switchSelectedFragment(shoppingListFragment)
                //R.id.ic_barcode -> switchSelectedFragment(scannerFragment)
                R.id.ic_profile -> switchSelectedFragment(profileFragment)
            }
            true
        }

        if (!Places.isInitialized()) {
            Places.initialize(this, getString(R.string.shuatt_google_api_key), Locale("es", "ES"))
        }
    }

    private fun switchSelectedFragment(fragment: Fragment) = supportFragmentManager.beginTransaction().apply {
        replace(R.id.fl_wrapper, fragment)
        commit()
    }

    fun sendMetricFirebaseAnalytics(event: String) {
        firebaseAnalytics.logEvent(event, Bundle())
    }

    fun getPostcode() : String {
        return postcode
    }

    fun goToAddProductView() {
        tab_bar_navigation.visibility = View.GONE
        val productsFragment = ProductsFragment()
        switchSelectedFragment(productsFragment)
    }

    fun returnToShoppingListFragment() {
        tab_bar_navigation.visibility = View.VISIBLE
        val shoppingListFragment = ShoppingListFragment()
        switchSelectedFragment(shoppingListFragment)
    }

    fun returnToUserProfileFragment() {
        tab_bar_navigation.visibility = View.VISIBLE
        val userProfileFragment = ProfileFragment()
        switchSelectedFragment(userProfileFragment)
    }

    fun goBackToLogin() {
        val loginIntent : Intent = Intent(this, LoginActivity::class.java).apply {
            print("switching to Login")
        }
        startActivity(loginIntent)
    }

    fun goToProviderShoppingCartResults() {
        tab_bar_navigation.visibility = View.GONE
        shoppingCartController.searchShoppingCarts(postcode, shoppingList)
        val providerShoppingCartResultsFragment = ProviderShoppingCartsFragment()
        switchSelectedFragment(providerShoppingCartResultsFragment)
    }

    fun goToEditProductTypeView(productTypeDto: ProductTypeDto, buttonText: String = "Añadir") {
        val dialogBuilder = AlertDialog.Builder(this)
        val editItemPopup = layoutInflater.inflate(R.layout.edit_product_popup, null)

        val productTypeName = editItemPopup.findViewById<EditText>(R.id.product_name_in_edit_view)
        productTypeName.setText(productTypeDto.name)
        val productQuantity = editItemPopup.findViewById<EditText>(R.id.quantity_product_type_text)
        var quantity = if (productTypeDto.quantity == 0) 1 else productTypeDto.quantity
        productQuantity.setText(quantity.toString())

        val productImage = editItemPopup.findViewById<ImageView>(R.id.product_icon_in_edit_view)
        firebaseStorage.reference.child("products/images/" + productTypeDto.itemId + ".png")
            .getBytes(1 * 1024 * 1024).addOnSuccessListener {image ->
                val options = BitmapFactory.Options()
                val bitmap = BitmapFactory.decodeByteArray(image, 0, image.size, options)
                productImage.setImageBitmap(bitmap)
            }

        dialogBuilder.setView(editItemPopup)
        val dialog = dialogBuilder.create()

        val closeView = editItemPopup.findViewById<ImageButton>(R.id.close_edit_product_view)
        closeView.setOnClickListener {
            dialog.dismiss()
        }

        val minusOne = editItemPopup.findViewById<ImageButton>(R.id.icon_minus_one_item_product)
        minusOne.setOnClickListener {
            if (quantity > 0) {
                quantity--
                productQuantity.setText(quantity.toString())
            }
        }

        val plusOne = editItemPopup.findViewById<ImageButton>(R.id.icon_add_one_item_product)
        plusOne.setOnClickListener {
            quantity++
            productQuantity.setText(quantity.toString())
        }

        val removeProductType = editItemPopup.findViewById<ImageButton>(R.id.remove_product_in_view)
        removeProductType.setOnClickListener {
            productTypeDto.quantity = 0
            this.shoppingListController.updateShoppingListProductType(productTypeDto)
            dialog.dismiss()
        }

        val editProductType = editItemPopup.findViewById<Button>(R.id.edit_button_to_shopping_list)
        editProductType.text = buttonText
        editProductType.setOnClickListener {
            productTypeDto.quantity = quantity
            this.shoppingListController.updateShoppingListProductType(productTypeDto)
            dialog.dismiss()
        }

        dialog.show()
    }

    fun setPostcodeToCompare(shoppingList : List<ProductTypeDto>) {
        this.shoppingList = shoppingList
        val dialogBuilder = AlertDialog.Builder(this)
        val setPostcodePopupView = layoutInflater.inflate(R.layout.set_postcode_popup, null)

        dialogBuilder.setView(setPostcodePopupView)
        val dialog = dialogBuilder.create()

        val inputText = setPostcodePopupView.findViewById<TextInputLayout>(R.id.postcode_input_text)
        val cancelButton = setPostcodePopupView.findViewById<Button>(R.id.postcode_popup_cancel_btn)
        cancelButton.setOnClickListener {
            dialog.dismiss()
        }

        val acceptButton = setPostcodePopupView.findViewById<Button>(R.id.postcode_popup_accept_btn)
        acceptButton.setOnClickListener {
            val postcode = inputText.editText?.text
            if (postcode != null && "" != postcode.toString()) {
                this.postcode = postcode.toString()
                dialog.dismiss()
                sendMetricFirebaseAnalytics(FirebaseMetricsName.compareShoppingList())
                val shoppingListText = shoppingList.fold("Shopping list: \n") {
                        shoppingListText, nextProduct -> shoppingListText + nextProduct.name + ", "
                }
                val user = userLoginRegisterController.getUser()
                val email = user?.email
                val slackMessage = "Shopping list: \n $shoppingListText \n user: $email \n postcode: $postcode \n in ANDROID"
                shauttSlackMessageService.sendSlackMessage(SlackChannel.SEARCH, slackMessage)
                goToProviderShoppingCartResults()
            } else {
                dialog.dismiss()
            }
        }

        dialog.show()
    }

    fun goToShoppingCartView(shoppingCartCandidate: ShoppingCartCandidateDto) {
        tab_bar_navigation.visibility = View.GONE
        val shoppingCartFragment = ShoppingCartFragment(shoppingCartController, firebaseStorage)
        shoppingCartFragment.setShoppingCart(shoppingCartCandidate)
        shoppingCartController.setShoppingCartViewWithShoppingCart(shoppingCartFragment, shoppingCartCandidate)
        switchSelectedFragment(shoppingCartFragment)
    }

    fun updateShoppingCartView() {
        val shoppingCart = shoppingCartController.getCurrentShoppingCart()
        goToShoppingCartView(shoppingCart)
    }

    fun editShoppingCartView(shoppingCartCandidate: ShoppingCartCandidateDto) {
        tab_bar_navigation.visibility = View.GONE
        val editShoppingCartFragment = EditShoppingCartFragment()
        editShoppingCartFragment.setShoppingCartEditable(shoppingCartCandidate.clone())
        switchSelectedFragment(editShoppingCartFragment)
    }

    fun goToEditShoppingCartProductView(shoppingCart: ShoppingCartCandidateDto, position: Int) {
        val dialogBuilder = AlertDialog.Builder(this)
        val editShoppingCartProductPopup = layoutInflater.inflate(R.layout.edit_single_shopping_cart_product_popup, null)

        val editShoppingCartProductRecyclerView = editShoppingCartProductPopup.findViewById<RecyclerView>(R.id.edit_single_shopping_cart_product_collection)

        val mLayoutManager = LinearLayoutManager(this)
        mLayoutManager.orientation = LinearLayoutManager.HORIZONTAL
        editShoppingCartProductRecyclerView.layoutManager = mLayoutManager
        editShoppingCartProductRecyclerView.itemAnimator = DefaultItemAnimator()

        val editShoppingCartFragment = EditShoppingCartFragment()
        editShoppingCartFragment.setShoppingCartEditable(shoppingCart.clone())

        editShoppingCartProductRecyclerView.adapter = EditableShoppingCartProductCellAdapter(this, editShoppingCartFragment, position, shoppingCart.shoppingCartProducts[position].otherOptionsProducts)

        val shoppingCartProductQuantity = editShoppingCartProductPopup.findViewById<EditText>(R.id.quantity_shopping_cart_product_text)
        var quantity = shoppingCart.shoppingCartProducts[position].quantity
        shoppingCartProductQuantity.setText(quantity.toString())

        dialogBuilder.setView(editShoppingCartProductPopup)
        val dialog = dialogBuilder.create()

        val closeView = editShoppingCartProductPopup.findViewById<ImageButton>(R.id.close_edit_shopping_cart_product_view)
        closeView.setOnClickListener {
            dialog.dismiss()
        }

        val minusOne = editShoppingCartProductPopup.findViewById<ImageButton>(R.id.icon_minus_one_more_shopping_cart_product)
        minusOne.setOnClickListener {
            if (quantity > 0) {
                quantity--
                shoppingCartProductQuantity.setText(quantity.toString())
            }
        }

        val plusOne = editShoppingCartProductPopup.findViewById<ImageButton>(R.id.icon_add_one_more_shopping_cart_product)
        plusOne.setOnClickListener {
            quantity++
            shoppingCartProductQuantity.setText(quantity.toString())
        }

        val editProductType = editShoppingCartProductPopup.findViewById<ImageButton>(R.id.accept_edit_shopping_cart_product_in_view)
        editProductType.setOnClickListener {
            shoppingCart.shoppingCartProducts[position].quantity = quantity
            shoppingCart.shoppingCartProducts[position].selectedProduct = editShoppingCartFragment.getTemporalShoppingCart().shoppingCartProducts[position].selectedProduct
            shoppingCartController.updateShoppingCart(shoppingCart)
            dialog.dismiss()
            updateShoppingCartView()
        }

        dialog.show()
    }

    fun goToCustomerDataView(shoppingCart: ShoppingCartCandidateDto, masterCartDto: MasterCartDto) {
        tab_bar_navigation.visibility = View.GONE
        val customerDataFragment = CustomerDataFragment(shoppingCartController, firebaseStorage)
        shoppingCartController.setCustomerDataView(customerDataFragment)
        customerDataFragment.setShoppingCartMasterCart(shoppingCart, masterCartDto)
        switchSelectedFragment(customerDataFragment)
    }

    fun goToAddAddressView() {
        tab_bar_navigation.visibility = View.GONE
        val addressCustomerDataFragment = AddressCustomerDataFragment(shoppingCartController, firebaseStorage)
        switchSelectedFragment(addressCustomerDataFragment)
    }

    fun goToDeliveryTimeView(masterCartDto: MasterCartDto) {
        tab_bar_navigation.visibility = View.GONE
        val deliveryTimeCustomerDataFragment = DeliveryTimeCustomerDataFragment(shoppingCartController, masterCartDto)
        shoppingCartController.setDeliveryTimeView(deliveryTimeCustomerDataFragment)
        switchSelectedFragment(deliveryTimeCustomerDataFragment)
    }

    fun goToConfirmationPage() {
        tab_bar_navigation.visibility = View.GONE
        val confirmationOrderFragment = ConfirmationOrderFragment(shoppingCartController, firebaseStorage)
        switchSelectedFragment(confirmationOrderFragment)
    }

    fun goToMyOrders() {
        tab_bar_navigation.visibility = View.GONE
        val myOrdersFragment = MyOrdersFragment(userProfileController)
        switchSelectedFragment(myOrdersFragment)
    }

    fun logoutUser() {
        userLoginRegisterController.logoutUser()
    }

    fun showCustomAlert(title: String, text: String) {
        val builder = AlertDialog.Builder(this)
        builder.setTitle(title)
        builder.setMessage(text)
        builder.setPositiveButton("Ok") { dialogInterface: DialogInterface, _: Int ->
            dialogInterface.dismiss()
        }
        val dialog : AlertDialog = builder.create()
        dialog.show()
    }
}