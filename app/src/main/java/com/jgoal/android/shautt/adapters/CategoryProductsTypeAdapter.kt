package com.jgoal.android.shautt.adapters

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import com.google.firebase.storage.FirebaseStorage
import com.jgoal.android.shautt.R
import com.jgoal.android.shautt.TabBarActivity
import com.jgoal.android.shautt.commons.FirebaseMetricsName
import com.jgoal.android.shautt.model.CategoryProductTypesDTO
import kotlin.math.roundToInt

class CategoryProductsTypeAdapter constructor(private val context: Context,
                                              private val firebaseStorage: FirebaseStorage,
                                              private val categoryProductTypes: List<CategoryProductTypesDTO>) : BaseExpandableListAdapter() {

    override fun getGroup(position: Int): Any {
        return categoryProductTypes[position]
    }

    override fun isChildSelectable(position: Int, expandedListPosition: Int): Boolean {
        return true
    }

    override fun hasStableIds(): Boolean {
        return false
    }

    override fun getGroupView(position: Int, isExpanded: Boolean, convertView: View?, viewGroup: ViewGroup?): View {
        val layoutInflater = LayoutInflater.from(context)
        val rowCategoryProductType = layoutInflater.inflate(R.layout.products_type_row, viewGroup, false)

        val posCategoryNameTextView = rowCategoryProductType.findViewById<TextView>(R.id.category_row_name)
        posCategoryNameTextView.text = categoryProductTypes[position].name
        return rowCategoryProductType!!
    }

    override fun getChildrenCount(position: Int): Int {
        return 1
    }

    override fun getChild(position: Int, expandedListPosition: Int): Any {
        return categoryProductTypes[position].items[expandedListPosition]
    }

    override fun getGroupId(position: Int): Long {
        return position.toLong()
    }

    override fun getChildView(position: Int, expandedListPosition: Int, isLastChild: Boolean, convertView: View?, viewGroup: ViewGroup?): View {val layoutInflater = LayoutInflater.from(context)
        val rowCategoryProductType = layoutInflater.inflate(R.layout.products_type_row, viewGroup, false)

        val posCategoryNameTextView = rowCategoryProductType.findViewById<TextView>(R.id.category_row_name)
        posCategoryNameTextView.text = categoryProductTypes[position].name
        val rowGridView = rowCategoryProductType.findViewById<GridView>(R.id.products_type_collection)

        rowGridView.adapter = ProductTypeCellAdapter(context, firebaseStorage, categoryProductTypes[position].items)
        val cellSize = 112
        val resColumns = if ((categoryProductTypes[position].items.size % 4) > 0) 1 else 0
        val rowHeight = (categoryProductTypes[position].items.size * cellSize) / 4
        val gridHeight = context.resources.displayMetrics.density * (rowHeight + resColumns * cellSize)
        rowGridView.layoutParams.height = gridHeight.roundToInt()

        rowGridView.setOnItemClickListener { _, _, clickedPos, _ ->
            val pos = position
            val expandedListPos = expandedListPosition
            val click = clickedPos
            Log.d("ENTRO", "CLIKCIK")
            val parentActivity = context as TabBarActivity
            parentActivity.sendMetricFirebaseAnalytics(FirebaseMetricsName.addProductToShoppingList())
            parentActivity.goToEditProductTypeView(categoryProductTypes[position].items[clickedPos])
        }
        return rowGridView
    }

    override fun getChildId(groupPosition: Int, expandedListPosition: Int): Long {
        return expandedListPosition.toLong()
    }

    override fun getGroupCount(): Int {
        return categoryProductTypes.size
    }
}