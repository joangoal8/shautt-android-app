package com.jgoal.android.shautt.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.TextView
import com.jgoal.android.shautt.R
import com.jgoal.android.shautt.model.ScheduleDeliveryTimeDto

class DeliveryTimeDaysAdapter(private val context: Context,
                              private val selectedDayPos: Int,
                              private val days: List<ScheduleDeliveryTimeDto>) : BaseAdapter() {

    override fun getCount(): Int {
        return days.size
    }

    override fun getItem(position: Int): Any {
        return days[position]
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getView(position: Int, convertView: View?, viewGroup: ViewGroup?): View {
        val layoutInflater = LayoutInflater.from(context)
        val rowDay = layoutInflater.inflate(R.layout.selected_day_delivery_time_row, viewGroup, false)

        val dayOfWeek = rowDay.findViewById<TextView>(R.id.day_of_week_delivery_time)
        dayOfWeek.text = days[position].dayOfWeek

        val dateDay = rowDay.findViewById<TextView>(R.id.day_date_delivery_time)
        dateDay.text = days[position].day

        if (position == selectedDayPos) {
            rowDay.setBackgroundResource(R.drawable.rounded_shautt_color_border_blank_background)
        }

        return rowDay
    }
}