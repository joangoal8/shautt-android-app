package com.jgoal.android.shautt.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.TextView
import com.jgoal.android.shautt.R
import com.jgoal.android.shautt.model.ScheduleDayDto

class DeliveryTimeHoursAdapter(private val context: Context,
                               private val times: List<ScheduleDayDto>) : BaseAdapter() {

    override fun getCount(): Int {
        return times.size
    }

    override fun getItem(position: Int): Any {
        return times[position]
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getView(position: Int, convertView: View?, viewGroup: ViewGroup?): View {
        val layoutInflater = LayoutInflater.from(context)
        val rowHour = layoutInflater.inflate(R.layout.option_hours_delivery_time_row, viewGroup, false)

        val hour = rowHour.findViewById<TextView>(R.id.hour_time_delivery_time)
        if (times[position].time != null) {
            hour.text = times[position].time?.substringAfterLast("-")
        }

        val fee = rowHour.findViewById<TextView>(R.id.hour_fee_delivery_time)
        if (times[position].amount != null) {
            val text = (times[position].amount!! / 100).toString() + "€"
            fee.text = text
        }

        return rowHour
    }
}