package com.jgoal.android.shautt.adapters

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.GridView
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.jgoal.android.shautt.R
import com.jgoal.android.shautt.TabBarActivity
import com.jgoal.android.shautt.model.ShoppingCartProductTypeDto
import com.jgoal.android.shautt.shoppingcart.EditShoppingCartView

class EditShoppingCartProductsAdapter(private val context: Context,
                                      private val editView: EditShoppingCartView,
                                      private val shoppingCartProducts: List<ShoppingCartProductTypeDto>) : BaseAdapter() {

    override fun getCount(): Int {
        return shoppingCartProducts.size
    }

    override fun getItem(position: Int): Any {
        return shoppingCartProducts[position]
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getView(position: Int, convertView: View?, viewGroup: ViewGroup?): View {
        val layoutInflater = LayoutInflater.from(context)
        val editShoppingCartProductsRow = layoutInflater.inflate(R.layout.edit_shopping_cart_products_row, viewGroup, false)

        val editShoppingCartRecyclerView = editShoppingCartProductsRow.findViewById<RecyclerView>(R.id.edit_shopping_cart_product_collection)
        val mLayoutManager = LinearLayoutManager(context)
        mLayoutManager.orientation = LinearLayoutManager.HORIZONTAL
        editShoppingCartRecyclerView.layoutManager = mLayoutManager
        editShoppingCartRecyclerView.itemAnimator = DefaultItemAnimator()
        editShoppingCartRecyclerView.adapter = EditableShoppingCartProductCellAdapter(context, editView, position, shoppingCartProducts[position].otherOptionsProducts)

        return editShoppingCartProductsRow
    }
}