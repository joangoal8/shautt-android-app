package com.jgoal.android.shautt.adapters

import android.content.Context
import android.content.res.ColorStateList
import android.graphics.Color
import android.graphics.drawable.GradientDrawable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.jgoal.android.shautt.R
import com.jgoal.android.shautt.model.ShoppingCartProductDto
import com.jgoal.android.shautt.shoppingcart.EditShoppingCartView
import com.squareup.picasso.Picasso

class EditableShoppingCartProductCellAdapter(private val context: Context,
                                             private val editView: EditShoppingCartView,
                                             private val productPosition: Int,
                                             private val shoppingCartProducts: List<ShoppingCartProductDto>) : RecyclerView.Adapter<EditableShoppingCartProductCellAdapter.MyViewHolder>() {

    inner class MyViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var prodImageView: ImageView = view.findViewById(R.id.edit_shopping_cart_product_cell_img)
        var productNameTextView: TextView = view.findViewById(R.id.edit_shopping_cart_product_cell_name)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val layoutInflater = LayoutInflater.from(context)
        val editableCellProduct = layoutInflater.inflate(R.layout.edit_shopping_cart_collection_cell, parent, false)

        return MyViewHolder(editableCellProduct)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        Picasso.get().load(shoppingCartProducts[position].imageUrl).into(holder.prodImageView)

        holder.productNameTextView.text = shoppingCartProducts[position].name

        holder.itemView.setOnClickListener {
            holder.itemView.setBackgroundColor(Color.parseColor("#FF375F"))
            editView.editTemporalShoppingCart(shoppingCartProducts[holder.layoutPosition], productPosition)
        }

    }

    override fun getItemCount(): Int {
        return shoppingCartProducts.size
    }
}