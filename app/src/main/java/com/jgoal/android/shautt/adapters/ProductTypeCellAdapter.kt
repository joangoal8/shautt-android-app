package com.jgoal.android.shautt.adapters

import android.content.Context
import android.graphics.BitmapFactory
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.ImageView
import android.widget.TextView
import com.google.firebase.storage.FirebaseStorage
import com.jgoal.android.shautt.R
import com.jgoal.android.shautt.model.ProductTypeDto

class ProductTypeCellAdapter constructor(private val context: Context,
                                         private val firebaseStorage: FirebaseStorage,
                                         private val productTypes: List<ProductTypeDto>) : BaseAdapter() {

    override fun getCount(): Int {
        return productTypes.size
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getItem(position: Int): Any {
        return productTypes[position]
    }

    override fun getView(position: Int, convertView: View?, viewGroup: ViewGroup?): View {
        val layoutInflater = LayoutInflater.from(context)
        val rowShoppingList = layoutInflater.inflate(R.layout.product_type_collection_cell, viewGroup, false)

        val posImageView = rowShoppingList.findViewById<ImageView>(R.id.product_type_cell_img)
        firebaseStorage.reference.child("products/images/" + productTypes[position].itemId + ".png")
            .getBytes(1 * 1024 * 1024).addOnSuccessListener {image ->
                val options = BitmapFactory.Options()
                val bitmap = BitmapFactory.decodeByteArray(image, 0, image.size, options)
                posImageView.setImageBitmap(bitmap)
            }
        val posNameTextView = rowShoppingList.findViewById<TextView>(R.id.product_type_cell_name)
        posNameTextView.text = productTypes[position].name
        return rowShoppingList
    }
}