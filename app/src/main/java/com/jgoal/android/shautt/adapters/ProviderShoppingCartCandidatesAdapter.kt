package com.jgoal.android.shautt.adapters

import android.content.Context
import android.graphics.BitmapFactory
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.ImageView
import android.widget.TextView
import com.google.firebase.storage.FirebaseStorage
import com.jgoal.android.shautt.R
import com.jgoal.android.shautt.model.ShoppingCartCandidateDto

class ProviderShoppingCartCandidatesAdapter(private val context: Context,
                                            private val firebaseStorage: FirebaseStorage,
                                            private val shoppingCarts: List<ShoppingCartCandidateDto>) : BaseAdapter() {
    override fun getCount(): Int {
        return shoppingCarts.size
    }

    override fun getItem(position: Int): Any {
        return shoppingCarts[position]
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getView(position: Int, convertView: View?, viewGroup: ViewGroup?): View {
        val layoutInflater = LayoutInflater.from(context)
        val rowProviderShoppingCart = layoutInflater.inflate(R.layout.providers_shopping_carts_row, viewGroup, false)

        val storeImage1 = rowProviderShoppingCart.findViewById<ImageView>(R.id.storeLogo1)

        firebaseStorage.reference.child("stores/" + shoppingCarts[position].stores[0].name + ".png")
                .getBytes(1 * 1024 * 1024).addOnSuccessListener {image ->
                    val options = BitmapFactory.Options()
                    val bitmap = BitmapFactory.decodeByteArray(image, 0, image.size, options)
                    storeImage1.setImageBitmap(bitmap)
        }

        if (shoppingCarts[position].stores.size > 1) {
            val storeImage2 = rowProviderShoppingCart.findViewById<ImageView>(R.id.storeLogo2)

            firebaseStorage.reference.child("stores/" + shoppingCarts[position].stores[1].name + ".png")
                .getBytes(1 * 1024 * 1024).addOnSuccessListener { image ->
                    val options = BitmapFactory.Options()
                    val bitmap = BitmapFactory.decodeByteArray(image, 0, image.size, options)
                    storeImage2.setImageBitmap(bitmap)
                }
        }

        val posQuantityTextView = rowProviderShoppingCart.findViewById<TextView>(R.id.shoppingCartPrice)
        val price = shoppingCarts[position].totalAmount.toString() + "€"
        posQuantityTextView.text = price

        return rowProviderShoppingCart
    }
}