package com.jgoal.android.shautt.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.ImageView
import android.widget.TextView
import com.jgoal.android.shautt.R
import com.jgoal.android.shautt.model.ShoppingCartProductTypeDto
import com.squareup.picasso.Picasso

class ShoppingCartProductsAdapter(private val context: Context,
                                  private val shoppingCartProducts: List<ShoppingCartProductTypeDto>) : BaseAdapter() {

    override fun getCount(): Int {
        return shoppingCartProducts.size
    }

    override fun getItem(position: Int): Any {
        return shoppingCartProducts[position]
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getView(position: Int, convertView: View?, viewGroup: ViewGroup?): View {
        val layoutInflater = LayoutInflater.from(context)
        val rowShoppingCart = layoutInflater.inflate(R.layout.selected_shopping_cart_row, viewGroup, false)

        val productImage = rowShoppingCart.findViewById<ImageView>(R.id.shoppingCartProductImage)

        Picasso.get().load(shoppingCartProducts[position].selectedProduct.imageUrl).into(productImage)

        val productNameTextView = rowShoppingCart.findViewById<TextView>(R.id.shoppingCartProductName)
        productNameTextView.text = shoppingCartProducts[position].selectedProduct.name

        val posPriceQuantityTextView = rowShoppingCart.findViewById<TextView>(R.id.shoppingCartProductPriceSummary)
        val priceQuantity = shoppingCartProducts[position].quantity.toString() + "x" + shoppingCartProducts[position].selectedProduct.price.toString() + "€"
        posPriceQuantityTextView.text = priceQuantity

        return rowShoppingCart
    }
}