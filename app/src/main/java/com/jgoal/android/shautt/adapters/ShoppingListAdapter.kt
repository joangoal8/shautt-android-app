package com.jgoal.android.shautt.adapters

import android.content.Context
import android.graphics.BitmapFactory
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.ImageView
import android.widget.TextView
import com.google.firebase.storage.FirebaseStorage
import com.jgoal.android.shautt.R
import com.jgoal.android.shautt.model.ProductTypeDto


class ShoppingListAdapter constructor(private val context: Context,
                                      private val firebaseStorage: FirebaseStorage,
                                      private val productTypes: List<ProductTypeDto>) : BaseAdapter() {

    override fun getCount(): Int {
        return productTypes.size
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getItem(position: Int): Any {
        return productTypes[position]
    }

    override fun getView(position: Int, convertView: View?, viewGroup: ViewGroup?): View {
        val layoutInflater = LayoutInflater.from(context)
        val rowShoppingList = layoutInflater.inflate(R.layout.shopping_list_row, viewGroup, false)

        val posImageView = rowShoppingList.findViewById<ImageView>(R.id.shopping_list_item_icon)
        firebaseStorage.reference.child("products/images/" + productTypes[position].itemId + ".png")
            .getBytes(1 * 1024 * 1024).addOnSuccessListener {image ->
                val options = BitmapFactory.Options()
                val bitmap = BitmapFactory.decodeByteArray(image, 0, image.size, options)
                posImageView.setImageBitmap(bitmap)
        }
        val posNameTextView = rowShoppingList.findViewById<TextView>(R.id.shopping_list_item_name)
        val posQuantityTextView = rowShoppingList.findViewById<TextView>(R.id.shopping_list_item_quantity)
        posNameTextView.text = productTypes[position].name
        posQuantityTextView.text = productTypes[position].quantity.toString()
        return rowShoppingList
    }

}