package com.jgoal.android.shautt.commons

import android.app.Activity
import androidx.appcompat.app.AlertDialog
import com.jgoal.android.shautt.R

class CustomSpinnerDialog constructor(private val activity: Activity) {

    private lateinit var dialog: AlertDialog

    fun startAlertDialog() {
        val builder = AlertDialog.Builder(activity)
        builder.setCancelable(false)

        val inflater = activity.layoutInflater
        builder.setView(inflater.inflate(R.layout.custom_spinner_loading, null))

        dialog = builder.create()
        dialog.show()
    }

    fun stopAlertDialog() {
        dialog.dismiss()
    }
}