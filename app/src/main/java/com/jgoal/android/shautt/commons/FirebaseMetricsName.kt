package com.jgoal.android.shautt.commons

class FirebaseMetricsName {

    companion object {

        fun listShoppingListProducts() : String = "list_shopping_list_products"

        fun addProductToShoppingList() : String = "add_product_shopping_list_manual"

        fun editProductToShoppingList() : String = "edit_product_shopping_list"

        fun clickCompareShoppingList() : String = "click_compare_shopping_list"

        fun compareProductToShoppingList() : String = "compare_product_shopping_list"

        fun scanBarcodeProduct() : String = "scan_barcode_product"

        fun editPostcode() : String = "edit_postcode"

        fun compareShoppingList() : String = "compare_shopping_list"

        fun choseStoreInShoppingList() : String = "chose_store_in_shopping_list"

        fun editSingleShoppingCartProduct() : String = "edit_single_shopping_cart_product"

        fun editAllShoppingCartProduct() : String = "edit_all_shopping_cart_product"

        fun confirmShoppingCart() : String = "confirm_shopping_cart"

        fun iniAddAddressToShoppingCart() : String = "ini_add_address_to_shopping_cart"

        fun addAddressToShoppingCart() : String = "add_address_to_shopping_cart"

        fun checkShoppingCartDeliveryTime() : String = "check_shopping_cart_delivery_time"

        fun getShoppingCartDayTime() : String = "get_shopping_cart_day_time"

        fun addShoppingCartDeliveryTime() : String = "add_shopping_cart_delivery_time"

        fun purchaseShoppingCart() : String = "purchase_shopping_cart"

        fun allowPushNotifications() : String = "allow_push_notifications"

        fun doesNotAllowPushNotifications() : String = "does_not_allow_push_notifications"

        fun errorPushNotifications() : String = "allow_push_notifications"

        fun errorRegisteringAndSettingUpUser() : String = "error_registering_user"

    }
}