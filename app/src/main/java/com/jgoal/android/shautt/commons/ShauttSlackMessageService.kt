package com.jgoal.android.shautt.commons

import android.util.Log
import com.jgoal.android.shautt.config.ShauttConfig
import com.jgoal.android.shautt.model.SlackChannel
import com.jgoal.android.shautt.model.SlackMessage
import com.jgoal.android.shautt.model.requests.MasterCartRegisterAddressRequest
import kotlinx.serialization.json.Json
import okhttp3.*
import java.io.IOException
import javax.inject.Inject

class ShauttSlackMessageService @Inject constructor(private val client: OkHttpClient) {

    private val registerChannelWebHook = "https://hooks.slack.com/services/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"
    private val appsEventsChannelWebHook = "https://hooks.slack.com/services/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"
    private val searchChannelWebHook = "https://hooks.slack.com/services/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"
    private val shoppingCartConfirmChannelWebHook = "https://hooks.slack.com/services/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"
    private val ordersChannelWebHook = "https://hooks.slack.com/services/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"

    private val json = Json { ignoreUnknownKeys = true }

    fun sendSlackMessage(channel: SlackChannel, message: String) {
        val urlString: String = when(channel) {
            SlackChannel.REGISTER -> registerChannelWebHook
            SlackChannel.SEARCH -> searchChannelWebHook
            SlackChannel.CONFIRM -> shoppingCartConfirmChannelWebHook
            SlackChannel.ORDER -> ordersChannelWebHook
        }

        val jsonBody = RequestBody.create(
            MediaType.parse("application/json"), json.encodeToString(
                SlackMessage.serializer(), SlackMessage(message)))

        val req = Request.Builder().post(jsonBody).url(urlString).build()
        client.newCall(req).enqueue(object : Callback {
            override fun onResponse(call: Call, response: Response) {
                print("MESSAGE SENT")
            }
            override fun onFailure(call: Call, e: IOException) {
                e.printStackTrace()
                Log.d("EXCEPTION", e.localizedMessage!!)
            }
        })
    }
}