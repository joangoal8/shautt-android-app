package com.jgoal.android.shautt.component

import com.jgoal.android.shautt.LoginActivity
import com.jgoal.android.shautt.RegisterActivity
import com.jgoal.android.shautt.TabBarActivity
import com.jgoal.android.shautt.modules.ShauttModule
import com.jgoal.android.shautt.shoppinglist.ProductsFragment
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [ShauttModule::class])
interface ShauttComponent {

    fun inject(target: LoginActivity)
    fun inject(target: RegisterActivity)
    fun inject(target: TabBarActivity)
    fun inject(target: ProductsFragment)
}