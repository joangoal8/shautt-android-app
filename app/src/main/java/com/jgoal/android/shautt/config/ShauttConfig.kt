package com.jgoal.android.shautt.config

class ShauttConfig {

    companion object {
        fun getFunctionsBaseUrl() : String = "https://europe-west6-shaut-tech.cloudfunctions.net/"

        fun getShauttBaseApi() : String = "https://shaut-tech.ew.r.appspot.com/shautt-engine/v1"
    }

}