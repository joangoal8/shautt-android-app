package com.jgoal.android.shautt.fragment

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.*
import android.widget.Button
import android.widget.ListView
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.fragment.app.Fragment
import com.jgoal.android.shautt.R
import com.jgoal.android.shautt.TabBarActivity
import com.jgoal.android.shautt.adapters.ShoppingListAdapter
import com.jgoal.android.shautt.commons.FirebaseMetricsName
import com.jgoal.android.shautt.model.ProductTypeDto
import com.jgoal.android.shautt.shoppinglist.ShoppingListView
import java.util.*

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [ShoppingListFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class ShoppingListFragment : Fragment(), ShoppingListView {
    private val shoppingListViewTitle = "Lista de la compra"

    private lateinit var shoppingListView : ListView
    private lateinit var tabBarActivity : TabBarActivity

    private var shoppingList = Collections.emptyList<ProductTypeDto>()

    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.shop_list_nav_bar_menu, menu)
        return super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_shopping_list, container, false)
        shoppingListView = view.findViewById(R.id.customer_shopping_list)
        val cActionBarToolbar : Toolbar = view.findViewById(R.id.custom_nav_bar)
        (requireActivity() as AppCompatActivity).setSupportActionBar(cActionBarToolbar)
        (requireActivity() as AppCompatActivity).supportActionBar?.title = shoppingListViewTitle
        tabBarActivity = (requireActivity() as AppCompatActivity) as TabBarActivity
        setUpActionToolbar(cActionBarToolbar, tabBarActivity)
        tabBarActivity.shoppingListController.setShoppingListView(this)
        getShoppingList(tabBarActivity)
        val searchProviderBtn = view.findViewById<Button>(R.id.compare_button)
        searchProviderBtn.setOnClickListener {
            tabBarActivity.sendMetricFirebaseAnalytics(FirebaseMetricsName.clickCompareShoppingList())
            tabBarActivity.setPostcodeToCompare(shoppingList)
        }
        return view
    }

    private fun setUpActionToolbar(toolbar: Toolbar, tabBarActivity: TabBarActivity){
        toolbar.setOnMenuItemClickListener { item ->
            when (item.itemId) {
                R.id.icon_sync_shop_list -> getShoppingList(tabBarActivity)
                R.id.icon_add_shop_list -> goToAddProduct()
                R.id.icon_share_shop_list -> shareShoppingList(tabBarActivity)
            }
            true
        }
    }

    private fun getShoppingList(tabBarActivity: TabBarActivity) {
        tabBarActivity.shoppingListController.retrieveShoppingList()
    }

    private fun goToAddProduct() {
        tabBarActivity.sendMetricFirebaseAnalytics(FirebaseMetricsName.listShoppingListProducts())
        tabBarActivity.goToAddProductView()
    }

    private fun shareShoppingList(tabBarActivity: TabBarActivity) {
        tabBarActivity.shoppingListController.getAndShareShoppingList()
    }

    override fun showShoppingList(shoppingList: List<ProductTypeDto>) {
        this.shoppingList = shoppingList
        shoppingListView.adapter = ShoppingListAdapter(tabBarActivity, tabBarActivity.firebaseStorage, shoppingList)
        shoppingListView.setOnItemClickListener { _, _, rowPos, _ ->
            tabBarActivity.goToEditProductTypeView(shoppingList[rowPos], "Editar")
        }
    }

    override fun displayError() {
        Log.d("ERROR", "ShoppingListFragment")
    }

    override fun shareShoppingList(shoppingList: List<ProductTypeDto>) {
        val intentShare = Intent(Intent.ACTION_SEND)
        intentShare.type = "text/plain"
        
        var shoppingListTxt = shoppingList.fold("La lista de la compra es: \n") {
            shoppingListText, nextProduct -> shoppingListText + "· " + nextProduct.name + "\n"
        }
        val subject = "Descargatela en: https://play.google.com/store/apps/details?id=com.jgoal.android.shautt"
        shoppingListTxt = shoppingListTxt + subject

        intentShare.putExtra(Intent.EXTRA_SUBJECT, subject)
        intentShare.putExtra(Intent.EXTRA_TEXT, shoppingListTxt)

        startActivity(Intent.createChooser(intentShare, "Comparte tu lista de la compra"))
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment shoppingListFragment.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            ShoppingListFragment().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }
}