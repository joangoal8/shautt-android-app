package com.jgoal.android.shautt.loginregister

interface LoginView {

    fun switchToTabBar()

    fun displayError()
}