package com.jgoal.android.shautt.loginregister

enum class ProviderLogin {
    EMAIL_PWD, GOOGLE
}