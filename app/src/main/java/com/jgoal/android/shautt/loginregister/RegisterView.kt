package com.jgoal.android.shautt.loginregister

interface RegisterView {

    fun isOKAndGoToTabBar()

    fun showError()
}