package com.jgoal.android.shautt.loginregister

import com.jgoal.android.shautt.model.UserDto

interface UserLoginRegisterController {

    fun setLoginView(loginView: LoginView)

    fun setRegisterView(registerView: RegisterView)

    fun loginUser(userId: String, email: String, providerLogin: ProviderLogin)

    fun configureUserWithShoppingBasket(userId: String, email: String)

    fun logoutUser()

    fun getUser(): UserDto?
}