package com.jgoal.android.shautt.loginregister

import android.util.Log
import com.jgoal.android.shautt.repository.UserFireStoreRepository
import com.jgoal.android.shautt.model.UserDto
import com.jgoal.android.shautt.user.UserViewModel
import javax.inject.Inject

class UserLoginRegisterControllerImpl @Inject constructor(private val userFireStoreRepository: UserFireStoreRepository,
                                                          private val userViewModel: UserViewModel
) : UserLoginRegisterController {

    private lateinit var loginView: LoginView
    private lateinit var registerView: RegisterView

    override fun setLoginView(loginView: LoginView) {
        this.loginView = loginView
    }

    override fun setRegisterView(registerView: RegisterView) {
        this.registerView = registerView
    }

    override fun loginUser(userId: String, email: String, providerLogin: ProviderLogin) {
        when (providerLogin) {
            ProviderLogin.EMAIL_PWD -> loginEmailPasswordUser(userId, email)
            ProviderLogin.GOOGLE -> registerSocialUserWithShoppingBasket(userId, email)
        }
    }

    private fun loginEmailPasswordUser(userId: String, email: String) {
        userFireStoreRepository.loginUser(userId, email).addOnSuccessListener { document ->
            if (document != null && document.data != null) {
                val shoppingListId: String = document.data?.get("shoppingBasketId") as String
                userViewModel.setUserDto(UserDto(userId, email, shoppingListId))
                loginView.switchToTabBar()
            }
        }
                .addOnFailureListener {
                    Log.d("ERROR", " Error Getting info login user from FireStore")
                    logoutUser()
                    loginView.displayError()
                }
    }

    override fun configureUserWithShoppingBasket(userId: String, email: String) {
        registerUserWithShoppingBasket(userId, email)
    }

    private fun registerSocialUserWithShoppingBasket(userId: String, email: String) {
        userFireStoreRepository.loginUser(userId, email).addOnSuccessListener { document ->
            if (document != null) {
                if (document.data != null && document.data?.get("shoppingBasketId") != null) {
                    userViewModel.setUserDto(UserDto(userId, email, document.data?.get("shoppingBasketId") as String))
                    loginView.switchToTabBar()
                } else {
                    registerSocialUserAndLinkShoppingBasket(userId, email)
                }
            } else {
                registerSocialUserAndLinkShoppingBasket(userId, email)
            }
        }
    }
    private fun registerUserWithShoppingBasket(userId: String, email: String) {
        userFireStoreRepository.configureUserWithShoppingBasket(userId, email)
                .addOnSuccessListener {
                    linkUserWithShoppingList(userId, email, it.id)
                }
                .addOnFailureListener {
                    //TODO Put analytics
                    Log.d("ERROR", " Error adding user to FireStore")
                    registerView.showError()
                }
    }

    private fun registerSocialUserAndLinkShoppingBasket(userId: String, email: String) {
        userFireStoreRepository.configureUserWithShoppingBasket(userId, email)
                .addOnSuccessListener {
                    linkUserWithShoppingList(userId, email, it.id)
                }
                .addOnFailureListener {
                    //TODO Put analytics
                    Log.d("ERROR", " Error adding user to FireStore")
                    registerView.showError()
                }
    }

    private fun linkUserWithShoppingList(userId: String, email: String, shoppingListId: String) {
        userFireStoreRepository.linkUserWithShoppingList(userId, email, shoppingListId)
            .addOnSuccessListener {
                Log.d("CUSTOM_LOG", "USER STORED IN FIREBASE")
                userViewModel.setUserDto(UserDto(userId, email, shoppingListId))
                registerView.isOKAndGoToTabBar()
            }
            .addOnFailureListener {
                //TODO Put analytics
                Log.d("ERROR", " Error linking shoppingList with user")
                registerView.showError()
            }
    }

    override fun logoutUser() {
        userViewModel.setUserDto(null)
    }

    override fun getUser(): UserDto? {
        return userViewModel.getUserDto()
    }
}