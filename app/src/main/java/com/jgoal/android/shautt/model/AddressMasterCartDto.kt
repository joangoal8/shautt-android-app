package com.jgoal.android.shautt.model

data class AddressMasterCartDto(val userAddressDto: UserAddressDto,
                                val postcode: String,
                                val addressMore: String,
                                val phone: String)

data class UserAddressDto(val street: String,
                          val city: String)