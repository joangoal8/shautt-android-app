package com.jgoal.android.shautt.model

import kotlinx.serialization.Serializable
import kotlinx.serialization.SerialName

@Serializable
data class CategoryProductTypesDTO(
    val name : String,
    var items : List<ProductTypeDto>,
    var isExpanded : Boolean = false
)