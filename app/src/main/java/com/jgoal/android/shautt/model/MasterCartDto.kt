package com.jgoal.android.shautt.model

import kotlinx.serialization.Serializable

@Serializable
data class MasterCartDto(val id: String,
                    val carts: List<CartDto>,
                    val providerProducts: List<MasterCartItemDto>,
                    val amount: Double,
                    var deliveryAddress: MasterCartDeliveryAddressDto? = null,
                    val quantity: Int,
                    val providerName: String,
                    var token: String? = null
) {
    @Serializable
    data class CartDto(val id: String,
                       val quantity : Int,
                       val nextAvailableDeliveryTime : String,
                       val providerStoreDto: ProviderStoreDto,
                       var deliveryTime : String? = null
    )

    @Serializable
    data class ProviderStoreDto(val name: String)

    @Serializable
    data class MasterCartDeliveryAddressDto(val id: String,
                                            val address: String,
                                            val city: String,
                                            val postalCode: String,
                                            val phone: String)
}

