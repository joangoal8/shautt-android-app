package com.jgoal.android.shautt.model

import kotlinx.serialization.Serializable

@Serializable
data class MasterCartItemDto(val itemId: String,
                             val productType: String,
                             val providerItemId: String,
                             val providerName: String, val quantity: Int)