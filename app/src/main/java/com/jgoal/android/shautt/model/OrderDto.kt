package com.jgoal.android.shautt.model

import kotlinx.serialization.Serializable

@Serializable
class OrderDto(val uuid: String,
               val amount: Double,
               val storeRef: String,
               val address: String,
               val city: String,
               val postalCode: String,
               val phone: String,
               val deliveryTime: DeliveryTimeDto,
               val providerOrderId: String? = null) {

    @Serializable
    class DeliveryTimeDto(val hour: Int,
                          val year: Int,
                          val month: String,
                          val dayOfMonth: Int,
                          val dayOfWeek: String,
                          val dayOfYear: Int,
                          val monthValue: Int) {

    }
}