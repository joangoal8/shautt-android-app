package com.jgoal.android.shautt.model

import kotlinx.serialization.Serializable

@Serializable
data class PaymentDataDto(val masterCart: Int,
                          val creditCard: String,
                          val creditCardExpirationMonth: Int,
                          val creditCardExpirationYear: Int,
                          val creditCardSecurity: String)