package com.jgoal.android.shautt.model

import kotlinx.serialization.Serializable
import kotlinx.serialization.SerialName

@Serializable
class ProductTypeDto(val itemId: String, @SerialName("name_es") val name: String, val categoryId : String, var quantity: Int = 0)