package com.jgoal.android.shautt.model

import kotlinx.serialization.Serializable

@Serializable
data class ScheduleDayDto(val time: String?,
                          val amount: Double?)