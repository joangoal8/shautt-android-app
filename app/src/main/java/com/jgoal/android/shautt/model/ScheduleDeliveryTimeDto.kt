package com.jgoal.android.shautt.model

import kotlinx.serialization.Serializable

@Serializable
data class ScheduleDeliveryTimeDto(val dayOfWeek: String,
                              val day: String,
                              val deliveryHours: List<ScheduleDayDto>)