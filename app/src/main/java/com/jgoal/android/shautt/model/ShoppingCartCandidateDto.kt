package com.jgoal.android.shautt.model

import kotlinx.serialization.Serializable

@Serializable
data class ShoppingCartCandidateDto (
    val shoppingCartProducts: List<ShoppingCartProductTypeDto>,
    var totalAmount: Double,
    val stores:  List<StoreDto>,
    val provider: String
) : Cloneable {

    public override fun clone(): ShoppingCartCandidateDto {
        val newShoppingCartProducts = ArrayList<ShoppingCartProductTypeDto>()
        shoppingCartProducts.forEach { shoppingCartProductTypeDto ->
            newShoppingCartProducts.add(shoppingCartProductTypeDto.clone())
        }
        val newStores = ArrayList<StoreDto>()
        stores.forEach { storeDto ->
            newStores.add(storeDto.clone())
        }
        return ShoppingCartCandidateDto(newShoppingCartProducts, totalAmount, newStores, provider)
    }
}

@Serializable
data class ShoppingCartProductTypeDto (
        var selectedProduct: ShoppingCartProductDto,
        val otherOptionsProducts: List<ShoppingCartProductDto>,
        var quantity: Int,
        val productType: ShoppingProductInfo
) : Cloneable {

    public override fun clone(): ShoppingCartProductTypeDto {
        val newOtherShoppingCartProducts = ArrayList<ShoppingCartProductDto>()
        otherOptionsProducts.forEach { shoppingCartProductDto ->
            newOtherShoppingCartProducts.add(shoppingCartProductDto.clone())
        }
        return ShoppingCartProductTypeDto(selectedProduct.clone(), newOtherShoppingCartProducts, quantity, productType.clone())
    }
}

@Serializable
data class StoreDto (
        val id: String,
        val name: String,
        val providerName: String,
        val providerId: String,
        val email: String,
        val minimumOrder: Int,
        val imageUrl: String,
        var categoryProvided: String?
) : Cloneable {

    public override fun clone(): StoreDto {
        return StoreDto(id, name, providerName, providerId, email, minimumOrder, imageUrl, categoryProvided)
    }
}

@Serializable
data class ShoppingCartProductDto (
        val id: String,
        val name: String,
        val providerName: String,
        val providerItemId: String,
        val price: Double,
        val imageUrl: String,
        val storeId: String,
        var productTypeProvided: String?,
        var varietyProvided: String?,
        var manufacturerName: String?,
        val limitUnits: Int,
        val productTypeCalculated: String,
        val score: Double
) : Cloneable {

    public override fun clone(): ShoppingCartProductDto {
        return ShoppingCartProductDto(id, name, providerName, providerItemId, price, imageUrl, storeId, productTypeProvided, varietyProvided, manufacturerName, limitUnits, productTypeCalculated, score)
    }
}
