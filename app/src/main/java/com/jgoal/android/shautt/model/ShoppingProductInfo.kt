package com.jgoal.android.shautt.model;

import kotlinx.serialization.Serializable;

@Serializable
data class ShoppingProductInfo(val productTypeName: String, val quantity: Int) : Cloneable {

    public override fun clone(): ShoppingProductInfo {
        return ShoppingProductInfo(productTypeName, quantity)
    }
}
