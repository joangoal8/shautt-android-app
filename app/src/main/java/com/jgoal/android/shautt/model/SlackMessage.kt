package com.jgoal.android.shautt.model

import kotlinx.serialization.Serializable

@Serializable
class SlackMessage(val text: String)

enum class SlackChannel {
    REGISTER, SEARCH, CONFIRM, ORDER
}