package com.jgoal.android.shautt.model

class UserDto(val id: String, val email: String, val shoppingListId: String)