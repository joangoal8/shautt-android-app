package com.jgoal.android.shautt.model.requests

import kotlinx.serialization.Serializable

@Serializable
class CartsTimeDeliveryRequest(val cartIds: List<String>,
                               val providerName: String)