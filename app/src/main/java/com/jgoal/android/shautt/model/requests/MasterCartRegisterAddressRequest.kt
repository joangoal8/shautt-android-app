package com.jgoal.android.shautt.model.requests

import com.jgoal.android.shautt.model.MasterCartItemDto
import kotlinx.serialization.Serializable

@Serializable
data class MasterCartRegisterAddressRequest(val userProviderAddress: UserProviderAddressRequest,
                                            val providerProducts: List<MasterCartItemDto>,
                                            val providerName: String,
                                            val token: String?)