package com.jgoal.android.shautt.model.requests

import com.jgoal.android.shautt.model.MasterCartItemDto
import kotlinx.serialization.Serializable

@Serializable
class MasterCartRequest(val storeIds: List<String>, val providerProductRequests : List<MasterCartItemDto>, val providerName: String)