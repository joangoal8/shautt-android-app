package com.jgoal.android.shautt.model.requests

enum class OperationAction {
    ADD, REMOVE, EDIT
}