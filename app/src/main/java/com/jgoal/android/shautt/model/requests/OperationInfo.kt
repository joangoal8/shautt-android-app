package com.jgoal.android.shautt.model.requests

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class OperationInfo(val operation: String, @SerialName("newItemInfo") val productTypeItemInfo: ProductTypeItemInfo? = null)