package com.jgoal.android.shautt.model.requests

import kotlinx.serialization.Serializable

@Serializable
data class ProductTypeItemInfo(val itemId: String, val quantity: Int, val lastUpdate: String)