package com.jgoal.android.shautt.model.requests

import com.jgoal.android.shautt.model.ShoppingProductInfo
import kotlinx.serialization.Serializable

@Serializable
class ProductsInfoRequest (val productTypes: List<ShoppingProductInfo>)