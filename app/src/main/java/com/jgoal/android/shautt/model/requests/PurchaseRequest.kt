package com.jgoal.android.shautt.model.requests

import com.jgoal.android.shautt.model.MasterCartItemDto
import com.jgoal.android.shautt.model.PaymentDataDto

import kotlinx.serialization.Serializable

@Serializable
class PurchaseRequest(val masterCartId: String,
                      val cartIds: List<String>,
                      val providerProducts: List<MasterCartItemDto>,
                      val cartDeliveryTime: CartDeliveryTime,
                      val providerPayment: PaymentDataDto,
                      val providerName: String,
                      var userId: String? = null) {

    @Serializable
    class CartDeliveryTime(val deliveryTime: String)
}