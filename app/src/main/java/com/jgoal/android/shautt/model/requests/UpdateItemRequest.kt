package com.jgoal.android.shautt.model.requests

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
class UpdateItemRequest(@SerialName("id") val shoppingListId: String, @SerialName("itemInfo") val productTypeItemInfo: ProductTypeItemInfo, val operationInfo: OperationInfo)



