package com.jgoal.android.shautt.model.requests

import kotlinx.serialization.Serializable

@Serializable
class UserProviderAddressRequest(val name: String,
                                 val address: String,
                                 val addressMore: String,
                                 val city: String,
                                 val postalCode: String,
                                 val phone: String,
                                 val comments: String = "")