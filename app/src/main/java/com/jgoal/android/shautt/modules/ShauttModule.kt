package com.jgoal.android.shautt.modules

import android.app.Application
import android.content.Context
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.functions.FirebaseFunctions
import com.google.firebase.storage.FirebaseStorage
import com.jgoal.android.shautt.loginregister.UserLoginRegisterController
import com.jgoal.android.shautt.loginregister.UserLoginRegisterControllerImpl
import com.jgoal.android.shautt.shoppinglist.ShoppingListController
import com.jgoal.android.shautt.shoppinglist.ShoppingListControllerImpl
import com.jgoal.android.shautt.user.UserViewModel
import com.squareup.picasso.Picasso
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import com.jgoal.android.shautt.repository.*
import com.jgoal.android.shautt.shoppingcart.ShoppingCartController
import com.jgoal.android.shautt.shoppingcart.ShoppingCartControllerImpl
import com.jgoal.android.shautt.userprofile.UserProfileController
import com.jgoal.android.shautt.userprofile.UserProfileControllerImpl
import com.jgoal.android.shautt.userprofile.UserProfileRepository
import com.jgoal.android.shautt.userprofile.UserProfileRepositoryImpl
import com.jgoal.android.shautt.commons.ShauttSlackMessageService

import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import java.util.concurrent.TimeUnit
import javax.inject.Singleton

@Module
class ShauttModule(private val app: Application) {

    @Provides
    @Singleton
    fun provideContext(): Context = app

    @Provides
    @Singleton
    fun provideFirebaseAuth(): FirebaseAuth = FirebaseAuth.getInstance()

    @Provides
    @Singleton
    fun provideFirebaseAnalytics() : FirebaseAnalytics = FirebaseAnalytics.getInstance(app)

    @Provides
    @Singleton
    fun provideFireStore() : FirebaseFirestore = FirebaseFirestore.getInstance()

    @Provides
    @Singleton
    fun provideFirebaseFunctions() : FirebaseFunctions = FirebaseFunctions.getInstance("europe-west6")

    @Provides
    @Singleton
    fun provideFirebaseStorage() : FirebaseStorage = FirebaseStorage.getInstance()

    @Provides
    @Singleton
    fun provideHttpClient(): OkHttpClient = OkHttpClient().newBuilder().readTimeout(1, TimeUnit.MINUTES)
            .writeTimeout(1, TimeUnit.MINUTES).build()

    @Provides
    @Singleton
    fun providePicasso() : Picasso = Picasso.get()

    @Provides
    @Singleton
    fun provideObjectMapper() : ObjectMapper = jacksonObjectMapper()

    @Provides
    @Singleton
    fun provideUserViewModel() : UserViewModel = UserViewModel()

    @Provides
    @Singleton
    fun provideUserFireStoreRepository(firebaseFirestore: FirebaseFirestore) : UserFireStoreRepository = UserFireStoreRepository(firebaseFirestore)

    @Provides
    @Singleton
    fun provideUserLoginRegisterController(userFireStoreRepository : UserFireStoreRepository, userViewModel: UserViewModel): UserLoginRegisterController = UserLoginRegisterControllerImpl(userFireStoreRepository, userViewModel)

    @Provides
    @Singleton
    fun provideShoppingListRepository(firebaseFunctions: FirebaseFunctions, okHttpClient: OkHttpClient, objectMapper: ObjectMapper) : ShoppingListRepository = ShoppingListRepositoryImpl(firebaseFunctions, okHttpClient, objectMapper)

    @Provides
    @Singleton
    fun provideShoppingListController(shoppingListRepository: ShoppingListRepository, userViewModel: UserViewModel) : ShoppingListController = ShoppingListControllerImpl(shoppingListRepository, userViewModel)

    @Provides
    @Singleton
    fun provideShoppingCartRepository(okHttpClient: OkHttpClient, objectMapper: ObjectMapper) : ShoppingCartRepository = ShoppingCartRepositoryImpl(okHttpClient, objectMapper)

    @Provides
    @Singleton
    fun provideShoppingCartController(shoppingCartRepository: ShoppingCartRepository, userViewModel: UserViewModel) : ShoppingCartController = ShoppingCartControllerImpl(shoppingCartRepository, userViewModel)

    @Provides
    @Singleton
    fun provideUserProfileRepository(okHttpClient: OkHttpClient) : UserProfileRepository = UserProfileRepositoryImpl(okHttpClient)

    @Provides
    @Singleton
    fun provideUserProfileController(userProfileRepository: UserProfileRepository, userViewModel: UserViewModel) : UserProfileController = UserProfileControllerImpl(userProfileRepository, userViewModel)

    @Provides
    @Singleton
    fun provideShauttSlackMessageService(okHttpClient: OkHttpClient) : ShauttSlackMessageService = ShauttSlackMessageService(okHttpClient)
}