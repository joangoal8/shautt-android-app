package com.jgoal.android.shautt.onboarding

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.jgoal.android.shautt.R
import com.jgoal.android.shautt.TabBarActivity

class OnBoardingActivity: AppCompatActivity() {

    private val layoutContentSlides = OnBoardingSlide.getSlides()
    private var index: Int = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.shautt_steps_onboarding_activity)

        setUpSlideFragment()
    }

    private fun setUpSlideFragment() {
        val slide = layoutContentSlides[index]
        switchSelectedFragment(OnBoardingStepFragment(this, slide))
    }

    fun goToNextSlide() {
        index++
        if (index < layoutContentSlides.size) {
            val slide = layoutContentSlides[index]
            switchSelectedFragment(OnBoardingStepFragment(this, slide))
        } else {
            switchToTabBar()
        }
    }

    private fun switchSelectedFragment(fragment: Fragment) = supportFragmentManager.beginTransaction().apply {
        replace(R.id.on_boarding_wrapper, fragment)
        commit()
    }

    private fun switchToTabBar() {
        val tabBarIntent : Intent = Intent(this, TabBarActivity::class.java).apply {
            print("switching to Tab Bar")
        }
        startActivity(tabBarIntent)
    }
}