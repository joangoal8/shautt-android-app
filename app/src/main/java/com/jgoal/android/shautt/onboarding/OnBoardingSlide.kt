package com.jgoal.android.shautt.onboarding

import com.jgoal.android.shautt.R

class OnBoardingSlide(val title: String,
                      val animationName: Int,
                      val description : String,
                      val buttonTitle: String) {

    companion object {
        fun getSlides() : List<OnBoardingSlide> {
            val slides = ArrayList<OnBoardingSlide>()
            slides.add(OnBoardingSlide("Paso 1", R.raw.shopping_checklist, "Añade tus productos a tu lista de la compra", "Paso 2"))
            slides.add(OnBoardingSlide("Paso 2", R.raw.analyzing, "Compara tu lista con los diferentes supermercados", "Paso 3"))
            slides.add(OnBoardingSlide("Paso 3", R.raw.mobile_shopping, "Elige la opción que más se adapte a tus necesidades", "Paso 4"))
            slides.add(OnBoardingSlide("Paso 4", R.raw.delivery_animation, "Recibe el pedido en tu casa", "Finalizar"))
            slides.add(OnBoardingSlide("Bienvenido 👋😍", R.raw.watermelon, "Empieza a añadir productos a tu lista", "Ir a mi lista"))
            return slides
        }
    }
}