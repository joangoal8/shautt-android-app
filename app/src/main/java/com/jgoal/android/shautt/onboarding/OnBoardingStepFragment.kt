package com.jgoal.android.shautt.onboarding

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import androidx.fragment.app.Fragment
import com.airbnb.lottie.LottieAnimationView
import com.jgoal.android.shautt.R

class OnBoardingStepFragment(private val context: OnBoardingActivity,
                             private val slide: OnBoardingSlide): Fragment() {

    override fun onCreateView(
            inflater: LayoutInflater, container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.steps_onboarding_fragment, container, false)

        val onBoardingTitle = view.findViewById<TextView>(R.id.step_slide_on_boarding_title)
        val onBoardingAnimation = view.findViewById<LottieAnimationView>(R.id.step_slide_on_boarding_animation)
        val onBoardingDescription = view.findViewById<TextView>(R.id.step_slide_on_boarding_description)
        val onBoardingButton = view.findViewById<Button>(R.id.step_slide_on_boarding_button)
        onBoardingTitle.text = slide.title
        onBoardingAnimation.setAnimation(slide.animationName)
        onBoardingDescription.text = slide.description
        onBoardingButton.text = slide.buttonTitle

        onBoardingButton.setOnClickListener {
            context.goToNextSlide()
        }
        return view
    }
}