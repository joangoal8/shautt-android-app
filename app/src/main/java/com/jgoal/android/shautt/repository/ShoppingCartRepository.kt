package com.jgoal.android.shautt.repository

import com.jgoal.android.shautt.model.AddressMasterCartDto
import com.jgoal.android.shautt.model.MasterCartDto
import com.jgoal.android.shautt.model.MasterCartItemDto
import com.jgoal.android.shautt.model.PaymentDataDto
import com.jgoal.android.shautt.model.ProductTypeDto
import okhttp3.Call

interface ShoppingCartRepository {

    fun searchShoppingCarts(postcode: String, shoppingList: List<ProductTypeDto>) : Call

    fun createMasterCart(postcode: String, storeIds: List<String>, masterCartItems: List<MasterCartItemDto>, provider: String) : Call

    fun registerMasterCart(masterCart: MasterCartDto, addressMasterCart: AddressMasterCartDto, token: String?) : Call

    fun retrieveDeliveryTime(token: String, cartIds: List<String>, providerName: String) : Call

    fun confirmPayment(token: String, masterCart: MasterCartDto, selectedTime: String, paymentDataDto: PaymentDataDto, userId: String?) : Call
}