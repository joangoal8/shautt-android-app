package com.jgoal.android.shautt.repository

import com.fasterxml.jackson.databind.ObjectMapper
import com.jgoal.android.shautt.config.ShauttConfig
import com.jgoal.android.shautt.model.*
import com.jgoal.android.shautt.model.requests.CartsTimeDeliveryRequest
import com.jgoal.android.shautt.model.requests.MasterCartRegisterAddressRequest
import com.jgoal.android.shautt.model.requests.MasterCartRequest
import com.jgoal.android.shautt.model.requests.ProductsInfoRequest
import com.jgoal.android.shautt.model.requests.PurchaseRequest
import com.jgoal.android.shautt.model.requests.UserProviderAddressRequest
import kotlinx.serialization.json.Json
import okhttp3.Call
import okhttp3.MediaType
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.RequestBody
import java.util.concurrent.TimeUnit
import javax.inject.Inject

class ShoppingCartRepositoryImpl @Inject constructor(private val client: OkHttpClient,
                                                     private val objectMapper: ObjectMapper) : ShoppingCartRepository {

    private val pathSearchShoppingCarts = "shoppingCarts"
    private val pathCreateShoppingCart = "shoppingCart"
    private val pathDeliveryTime = "shoppingCart/carts/deliveryTime"
    private val pathShoppingCartPurchase = "shoppingCart/purchase"

    private val key = "2BAE5BC7AEB968A18E6B7CFE698E8587"
    private val json = Json { ignoreUnknownKeys = true }

    override fun searchShoppingCarts(postcode: String, shoppingList: List<ProductTypeDto>) : Call  {
        val productsInfoRequest = buildProductsInfo(shoppingList)

        val jsonBody = RequestBody.create(
                MediaType.parse("application/json"), json.encodeToString(ProductsInfoRequest.serializer(), productsInfoRequest))

        val req = Request.Builder().post(jsonBody).url(ShauttConfig.getShauttBaseApi() + "/" + postcode + "/" + pathSearchShoppingCarts).addHeader("Content-Type", "application/json").addHeader("key", key).build()
        return client.newCall(req)
    }

    private fun buildProductsInfo(shoppingList: List<ProductTypeDto>) : ProductsInfoRequest {
        val productsInfo = ArrayList<ShoppingProductInfo>()
        shoppingList.forEach { product ->
            productsInfo.add(ShoppingProductInfo(product.name, product.quantity))
        }
        return ProductsInfoRequest(productsInfo)
    }

    override fun createMasterCart(postcode: String, storeIds: List<String>, masterCartItems: List<MasterCartItemDto>, provider: String): Call {
        val jsonBody = RequestBody.create(
                MediaType.parse("application/json"), json.encodeToString(MasterCartRequest.serializer(), MasterCartRequest(storeIds, masterCartItems, provider)))

        val req = Request.Builder().post(jsonBody).url(ShauttConfig.getShauttBaseApi() + "/" + pathCreateShoppingCart).addHeader("Content-Type", "application/json").addHeader("postcode", postcode).addHeader("key", key).build()
        return client.newCall(req)
    }

    override fun registerMasterCart(
        masterCart: MasterCartDto,
        addressMasterCart: AddressMasterCartDto,
        token: String?) : Call {
        val userProviderAddressRequest = UserProviderAddressRequest("Home " + addressMasterCart.userAddressDto.city ,
            addressMasterCart.userAddressDto.street, addressMasterCart.addressMore,
            addressMasterCart.userAddressDto.city, addressMasterCart.postcode, addressMasterCart.phone)

        val masterCartRegisterRequest = MasterCartRegisterAddressRequest(userProviderAddressRequest, masterCart.providerProducts, masterCart.providerName, token)

        val jsonBody = RequestBody.create(
            MediaType.parse("application/json"), json.encodeToString(MasterCartRegisterAddressRequest.serializer(), masterCartRegisterRequest))

        val req = Request.Builder().post(jsonBody).url(ShauttConfig.getShauttBaseApi() + "/" + pathCreateShoppingCart + "/" + masterCart.id + "/address")
            .addHeader("Content-Type", "application/json").addHeader("key", key).build()

        return client.newCall(req)
    }

    override fun retrieveDeliveryTime(token: String, cartIds: List<String>, providerName: String): Call {
        val jsonBody = RequestBody.create(
                MediaType.parse("application/json"), json.encodeToString(CartsTimeDeliveryRequest.serializer(), CartsTimeDeliveryRequest(cartIds, providerName)))
        val req = Request.Builder().post(jsonBody).url(ShauttConfig.getShauttBaseApi() + "/" + pathDeliveryTime).addHeader("Content-Type", "application/json").addHeader("Authorization", token).addHeader("key", key).build()
        return client.newCall(req)
    }

    override fun confirmPayment(token: String, masterCart: MasterCartDto, selectedTime: String, paymentDataDto: PaymentDataDto, userId: String?) : Call {
        val cartIds = masterCart.carts.map {
            it.id
        }
        val purchaseRequest = PurchaseRequest(masterCart.id, cartIds, masterCart.providerProducts,
                PurchaseRequest.CartDeliveryTime(selectedTime), paymentDataDto, masterCart.providerName)
        if (userId != null) {
            purchaseRequest.userId = userId
        }

        val jsonBody = RequestBody.create(
                MediaType.parse("application/json"), json.encodeToString(PurchaseRequest.serializer(), purchaseRequest))
        val req = Request.Builder().post(jsonBody).url(ShauttConfig.getShauttBaseApi() + "/" + pathShoppingCartPurchase).addHeader("Content-Type", "application/json").addHeader("Authorization", token).addHeader("key", key).build()
        return client.newCall(req)
    }
}