package com.jgoal.android.shautt.repository

import com.google.android.gms.tasks.Task
import com.jgoal.android.shautt.model.ProductTypeDto
import okhttp3.Call

interface ShoppingListRepository {

    fun retrieveShoppingList(shoppingListId : String) : Task<List<ProductTypeDto>>

    fun retrieveAllProductTypes() : Call

    fun editShoppingList(shoppingListId : String, productTypeDto: ProductTypeDto) : Task<List<ProductTypeDto>>
}