package com.jgoal.android.shautt.repository

import com.fasterxml.jackson.databind.ObjectMapper
import com.google.android.gms.tasks.Task
import com.google.firebase.functions.FirebaseFunctions
import com.jgoal.android.shautt.config.ShauttConfig
import com.jgoal.android.shautt.model.ProductTypeDto
import com.jgoal.android.shautt.model.requests.OperationAction
import com.jgoal.android.shautt.model.requests.OperationInfo
import com.jgoal.android.shautt.model.requests.ProductTypeItemInfo
import com.jgoal.android.shautt.model.requests.UpdateItemRequest
import kotlinx.serialization.json.*
import okhttp3.Call
import okhttp3.OkHttpClient
import okhttp3.Request
import java.time.format.DateTimeFormatter
import java.time.LocalDateTime
import javax.inject.Inject
import kotlin.collections.HashMap

class ShoppingListRepositoryImpl @Inject constructor(private val firebaseFunctions: FirebaseFunctions,
                                                     private val client: OkHttpClient,
                                                     private val objectMapper: ObjectMapper) : ShoppingListRepository {

    private val getShoppingBasketFun = "getShoppingBasket"
    private val getAllProductTypes = "getAllProductTypes"
    private val updateItemSB = "updateItemSB"
    private val dateFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")

    private val json = Json { ignoreUnknownKeys = true }

    override fun retrieveShoppingList(shoppingListId : String): Task<List<ProductTypeDto>> {
        val data = hashMapOf("id" to shoppingListId)

        return firebaseFunctions.getHttpsCallable(getShoppingBasketFun).call(data).continueWith { task ->
            val result = task.result?.data as List<HashMap<String, Any>>
            var shoppingListDto = ArrayList<ProductTypeDto>()
            result.forEach { hashMap ->
                val itemId = hashMap.get("itemId") as String
                val nameEs = hashMap.get("name_es") as String
                val categoryId = hashMap.get("categoryId") as String
                val quantity = hashMap.get("quantity") as Int
                shoppingListDto.add(ProductTypeDto(itemId, nameEs, categoryId, quantity))
            }
            shoppingListDto
        }
    }

    override fun retrieveAllProductTypes() : Call {
        val req = Request.Builder().get().url(ShauttConfig.getFunctionsBaseUrl() + getAllProductTypes).build()
        return client.newCall(req)
    }

    override fun editShoppingList(shoppingListId : String, productTypeDto: ProductTypeDto): Task<List<ProductTypeDto>> {
        val newItemInfo = ProductTypeItemInfo(productTypeDto.itemId, productTypeDto.quantity, LocalDateTime.now().format(dateFormatter))
        val operationInfo = OperationInfo(OperationAction.EDIT.toString(), newItemInfo)
        val editItemRequest = UpdateItemRequest(shoppingListId, newItemInfo, operationInfo)

        return updateItemToSB(shoppingListId, productTypeDto, editItemRequest)
    }

    private fun updateItemToSB(shoppingListId : String, productTypeDto: ProductTypeDto, updateItemRequest: UpdateItemRequest) : Task<List<ProductTypeDto>> {
        val jsonData = json.encodeToJsonElement(UpdateItemRequest.serializer(), updateItemRequest)
        val jsonMapData = mapJsonToHashMap(jsonData)

        return firebaseFunctions.getHttpsCallable(updateItemSB).call(jsonMapData).continueWith { task ->
            val result = task.result?.data as List<HashMap<String, Any>>
            var shoppingListDto = ArrayList<ProductTypeDto>()
            result.forEach { hashMap ->
                val itemId = hashMap.get("itemId") as String
                val nameEs = hashMap.get("name_es") as String
                val categoryId = hashMap.get("categoryId") as String
                val quantity = hashMap.get("quantity") as Int
                shoppingListDto.add(ProductTypeDto(itemId, nameEs, categoryId, quantity))
            }
            shoppingListDto
        }
    }

    private fun mapJsonToHashMap(jsonElement: JsonElement) : HashMap<String, Any> {
        var hashMap = HashMap<String, Any>()
        jsonElement.jsonObject.forEach { key, jsonElement ->
                if (jsonElement is JsonPrimitive) {
                    if (jsonElement.isString) {
                        hashMap.put(key, jsonElement.toString().replace("\"",""))
                    } else {
                        val jsonPrimitive = jsonElement as JsonPrimitive
                        hashMap.put(key, jsonPrimitive.int)
                    }
                } else {
                    hashMap.put(key, mapJsonToHashMap(jsonElement))
                }
        }
        return hashMap
    }
}