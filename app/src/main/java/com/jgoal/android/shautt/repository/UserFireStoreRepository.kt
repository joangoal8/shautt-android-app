package com.jgoal.android.shautt.repository

import android.util.Log
import com.google.android.gms.tasks.Task
import com.google.firebase.firestore.DocumentReference
import com.google.firebase.firestore.DocumentSnapshot
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.SetOptions
import com.jgoal.android.shautt.model.UserDto
import com.jgoal.android.shautt.user.UserViewModel
import javax.inject.Inject

class UserFireStoreRepository @Inject constructor(private val firebaseStore: FirebaseFirestore) : UserRepository {

    private val usersCollection = "users"
    private val shoppingListCollection = "shoppingBaskets"

    override fun loginUser(userId: String, email: String) : Task<DocumentSnapshot> {
         return firebaseStore.collection(usersCollection).document(userId).get()
    }

    override fun configureUserWithShoppingBasket(userId: String, email: String): Task<DocumentReference> {
        Log.d("", "Passoooo")
        return createNewShoppingListAndLinkToUser(userId, email)
    }

    private fun createNewShoppingListAndLinkToUser(userId: String, email: String): Task<DocumentReference> {
        return firebaseStore.collection(shoppingListCollection).add(hashMapOf<String, String>())
            .addOnSuccessListener {
                linkUserWithShoppingList(userId, email, it.id)
            }
            .addOnFailureListener {
                //TODO Put analytics
                Log.d("ERROR", " Error adding user to FireStore")
            }
    }

    override fun linkUserWithShoppingList(userId: String, email: String, shoppingListId: String): Task<Void> {
        val data = hashMapOf("shoppingBasketId" to shoppingListId)
        return firebaseStore.collection(usersCollection).document(userId).set(data, SetOptions.merge())
    }
}