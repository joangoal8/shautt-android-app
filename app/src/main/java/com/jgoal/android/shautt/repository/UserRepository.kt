package com.jgoal.android.shautt.repository

import com.google.android.gms.tasks.Task
import com.google.firebase.firestore.DocumentReference
import com.google.firebase.firestore.DocumentSnapshot

interface UserRepository {

    fun loginUser(userId: String, email: String) : Task<DocumentSnapshot>

    fun configureUserWithShoppingBasket(userId: String, email: String): Task<DocumentReference>

    fun linkUserWithShoppingList(userId: String, email: String, shoppingListId: String): Task<Void>
}