package com.jgoal.android.shautt.shoppingcart

import android.app.Activity
import android.content.ContentValues.TAG
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.Menu
import android.view.MenuInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.fragment.app.Fragment
import com.google.android.libraries.places.api.model.Place
import com.google.android.libraries.places.widget.Autocomplete
import com.google.android.libraries.places.widget.AutocompleteActivity
import com.google.android.libraries.places.widget.model.AutocompleteActivityMode
import com.google.android.material.textfield.TextInputLayout
import com.google.firebase.storage.FirebaseStorage
import com.jgoal.android.shautt.R
import com.jgoal.android.shautt.TabBarActivity
import com.jgoal.android.shautt.commons.FirebaseMetricsName
import com.jgoal.android.shautt.model.UserAddressDto

class AddressCustomerDataFragment(private val shoppingCartController: ShoppingCartController,
                                  private val firebaseStorage: FirebaseStorage): Fragment() {

    private val addressCustomerDataViewTitle = "Dirección de entrega"
    private val autoCompleteRequestCode = 1

    private lateinit var tabBarActivity : TabBarActivity
    private lateinit var streetAddressButton: Button

    private var userAddressDto: UserAddressDto? = null

    private lateinit var homeAddressInput: TextInputLayout
    private lateinit var doorAddressInput: TextInputLayout
    private lateinit var phoneAddressInput: TextInputLayout

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.products_items_menu, menu)
        return super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onCreateView(
            inflater: LayoutInflater, container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_address_customer_data, container, false)

        tabBarActivity = (requireActivity() as AppCompatActivity) as TabBarActivity

        val toolbar : Toolbar = view.findViewById(R.id.address_customer_data_custom_nav_bar)
        toolbar.title = addressCustomerDataViewTitle

        val menu: Menu = toolbar.menu

        streetAddressButton = view.findViewById(R.id.address_street_customer_data_button)
        streetAddressButton.setOnClickListener {
            val fields = listOf(Place.Field.ID, Place.Field.NAME, Place.Field.ADDRESS_COMPONENTS)

            val intent = Autocomplete.IntentBuilder(AutocompleteActivityMode.FULLSCREEN, fields)
                .build(tabBarActivity)
            startActivityForResult(intent, autoCompleteRequestCode)
        }

        homeAddressInput = view.findViewById(R.id.home_address_customer_data_input_text)
        doorAddressInput = view.findViewById(R.id.door_address_customer_data_input_text)
        phoneAddressInput = view.findViewById(R.id.phone_customer_data_input_text)

        val goBackToCustomerDataView = menu.findItem(R.id.icon_close_view_address_customer_data)
        goBackToCustomerDataView.setOnMenuItemClickListener {
            shoppingCartController.showCustomerMasterCart()
            true
        }

        val editCustomerDataAddress = menu.findItem(R.id.icon_accept_view_address_customer_data)
        editCustomerDataAddress.setOnMenuItemClickListener {
            validateAndSetAddressInfo()
            true
        }

        return view
    }

    private fun validateAndSetAddressInfo() {
        if ((userAddressDto == null || userAddressDto?.street == null || userAddressDto?.city == null
                    || userAddressDto?.street == "" || userAddressDto?.city == "")
            || (homeAddressInput.editText?.text == null || homeAddressInput.editText?.text.toString() == "")
            || (doorAddressInput.editText?.text == null || doorAddressInput.editText?.text.toString() == "")
            || (phoneAddressInput.editText?.text == null || phoneAddressInput.editText?.text.toString() == "")) {
            displayAddressCustomerDataAlert("Todos los campos tienen que ser rellenados")
        } else {
            val regex = "([+][0-9][0-9])?(([0-9]){9})".toRegex()
            if (!regex.matches(phoneAddressInput.editText?.text.toString())) {
                displayAddressCustomerDataAlert("Error con el número de teléfono proporcionado")
                return
            }
            tabBarActivity.sendMetricFirebaseAnalytics(FirebaseMetricsName.addAddressToShoppingCart())
            shoppingCartController.registerMasterCart(userAddressDto!!, homeAddressInput.editText?.text.toString() + " " + this.doorAddressInput.editText?.text.toString(),
                this.phoneAddressInput.editText?.text.toString())
        }
    }

    private fun displayAddressCustomerDataAlert(message: String) {
        val dialogBuilder = AlertDialog.Builder(tabBarActivity)
        val customerAlertPopupView = layoutInflater.inflate(R.layout.customer_data_alert_popup, null)

        dialogBuilder.setView(customerAlertPopupView)
        val dialog = dialogBuilder.create()

        val messageText = customerAlertPopupView.findViewById<TextView>(R.id.customer_data_alert_popup_description)
        messageText.text = message
        val acceptButton = customerAlertPopupView.findViewById<Button>(R.id.customer_data_popup_accept_btn)
        acceptButton.setOnClickListener {
            dialog.dismiss()
        }

        dialog.show()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == autoCompleteRequestCode) {
            when (resultCode) {
                Activity.RESULT_OK -> {
                    data?.let {
                        val place = Autocomplete.getPlaceFromIntent(data)
                        streetAddressButton.text = place.name
                        var locality: String? = null
                        place.addressComponents?.asList()?.forEach { component ->
                            component.types.forEach { type ->
                                when (type) {
                                    "locality" -> locality = component.name
                                }
                            }
                        }
                        if (locality != null) {
                            userAddressDto = UserAddressDto(place.name!!, locality!!)
                        }
                        Log.i(TAG, "Place: ${place.name}, ${place.id}")
                    }
                }
                AutocompleteActivity.RESULT_ERROR -> {
                    // TODO: Handle the error.
                    data?.let {
                        val status = Autocomplete.getStatusFromIntent(data)
                        Log.i(TAG, status.statusMessage!!)
                    }
                }
                Activity.RESULT_CANCELED -> {
                    // The user canceled the operation.
                }
            }
            return
        }
        super.onActivityResult(requestCode, resultCode, data)
    }
}