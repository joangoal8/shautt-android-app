package com.jgoal.android.shautt.shoppingcart

import android.graphics.BitmapFactory
import android.os.Bundle
import android.view.LayoutInflater
import android.view.Menu
import android.view.MenuInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.fragment.app.Fragment
import com.google.firebase.storage.FirebaseStorage
import com.jgoal.android.shautt.R
import com.jgoal.android.shautt.TabBarActivity

class ConfirmationOrderFragment(private val shoppingCartController: ShoppingCartController,
                                private val firebaseStorage: FirebaseStorage): Fragment() {

    private val confirmationViewTitle = "Confirmación"

    private lateinit var tabBarActivity : TabBarActivity

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.products_items_menu, menu)
        return super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onCreateView(
            inflater: LayoutInflater, container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_confirmation_order, container, false)

        tabBarActivity = (requireActivity() as AppCompatActivity) as TabBarActivity

        val toolbar : Toolbar = view.findViewById(R.id.confirmation_order_custom_nav_bar)
        toolbar.title = confirmationViewTitle

        val menu: Menu = toolbar.menu

        val closeShoppingFlowAndGoToShoppingList = menu.findItem(R.id.icon_back_close_shopping_cart_flow)
        closeShoppingFlowAndGoToShoppingList.setOnMenuItemClickListener {
            tabBarActivity.returnToShoppingListFragment()
            true
        }

        val shoppingCart = shoppingCartController.getCurrentShoppingCart()
        val orderDto = shoppingCartController.getOrderDto()
        val masterCartDto = shoppingCartController.getMasterCartDto()

        val addressTextView = view.findViewById<TextView>(R.id.address_confirmation_delivery_address)
        addressTextView.text = masterCartDto.deliveryAddress?.address

        val confirmationStoresTextView = view.findViewById<TextView>(R.id.confirmation_stores_names)
        confirmationStoresTextView.text = orderDto.storeRef

        val deliveryTimeTextView = view.findViewById<TextView>(R.id.confirmation_delivery_time_text)
        val deliveryTimeText = orderDto.deliveryTime.dayOfMonth.toString() + "/" + orderDto.deliveryTime.monthValue + "/" + orderDto.deliveryTime.year + " " + orderDto.deliveryTime.hour + ":00 h"
        deliveryTimeTextView.text = deliveryTimeText

        val confirmationTotalPriceTextView = view.findViewById<TextView>(R.id.order_total_amount_price)
        val priceConfirmed = orderDto.amount.toString() + " €"
        confirmationTotalPriceTextView.text = priceConfirmed

        val storeImageView1 = view.findViewById<ImageView>(R.id.confirmation_store_image_1)

        firebaseStorage.reference.child("stores/" + shoppingCart.stores[0].name + ".png")
                .getBytes(1 * 1024 * 1024).addOnSuccessListener {image ->
                    val options = BitmapFactory.Options()
                    val bitmap = BitmapFactory.decodeByteArray(image, 0, image.size, options)
                    storeImageView1.setImageBitmap(bitmap)
                }

        if (shoppingCart.stores.size > 1) {
            val storeImageView2 = view.findViewById<ImageView>(R.id.confirmation_store_image_2)

            firebaseStorage.reference.child("stores/" + shoppingCart.stores[1].name + ".png")
                    .getBytes(1 * 1024 * 1024).addOnSuccessListener { image ->
                        val options = BitmapFactory.Options()
                        val bitmap = BitmapFactory.decodeByteArray(image, 0, image.size, options)
                        storeImageView2.setImageBitmap(bitmap)
                    }
        }

        return view
    }
}