package com.jgoal.android.shautt.shoppingcart

import android.app.Activity
import android.graphics.BitmapFactory
import android.graphics.Color
import android.os.Bundle
import android.view.LayoutInflater
import android.view.Menu
import android.view.MenuInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.core.content.ContextCompat.getSystemService
import androidx.fragment.app.Fragment
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.textfield.TextInputLayout
import com.google.firebase.storage.FirebaseStorage
import com.jgoal.android.shautt.R
import com.jgoal.android.shautt.TabBarActivity
import com.jgoal.android.shautt.commons.FirebaseMetricsName
import com.jgoal.android.shautt.model.MasterCartDto
import com.jgoal.android.shautt.model.PaymentDataDto
import com.jgoal.android.shautt.model.ShoppingCartCandidateDto
import com.jgoal.android.shautt.model.SlackChannel
import java.util.*

class CustomerDataFragment(private val shoppingCartController: ShoppingCartController,
                           private val firebaseStorage: FirebaseStorage): Fragment(), CustomerDataView {

    private val customerDataViewTitle = "Tus datos"

    private lateinit var masterCartDto: MasterCartDto
    private lateinit var shoppingCart: ShoppingCartCandidateDto

    private lateinit var tabBarActivity : TabBarActivity

    private lateinit var creditCardNumber: TextInputLayout
    private lateinit var creditCardExpirationMonth: TextInputLayout
    private lateinit var creditCardExpirationYear: TextInputLayout
    private lateinit var creditCardCVV: TextInputLayout

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.products_items_menu, menu)
        return super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onCreateView(
            inflater: LayoutInflater, container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_customer_data, container, false)

        tabBarActivity = (requireActivity() as AppCompatActivity) as TabBarActivity

        val toolbar : Toolbar = view.findViewById(R.id.customer_data_custom_nav_bar)
        toolbar.title = customerDataViewTitle

        val menu: Menu = toolbar.menu

        val goBackToShoppingCartView = menu.findItem(R.id.icon_back_customer_data_view_to_shopping_cart)
        goBackToShoppingCartView.setOnMenuItemClickListener {
            shoppingCartController.setToken(null)
            tabBarActivity.updateShoppingCartView()
            true
        }

        val goToEditAddressView = view.findViewById<Button>(R.id.address_customer_data_button)
        goToEditAddressView.setOnClickListener {
            tabBarActivity.sendMetricFirebaseAnalytics(FirebaseMetricsName.iniAddAddressToShoppingCart())
            tabBarActivity.goToAddAddressView()
        }

        val goToDeliveryTimeView = view.findViewById<Button>(R.id.delivery_time_customer_data_button)
        goToDeliveryTimeView.setOnClickListener {
            tabBarActivity.sendMetricFirebaseAnalytics(FirebaseMetricsName.checkShoppingCartDeliveryTime())
            tabBarActivity.goToDeliveryTimeView(masterCartDto)
        }

        if (masterCartDto.deliveryAddress != null) {
            goToEditAddressView.text = masterCartDto.deliveryAddress?.address
        }

        if (masterCartDto.carts[0].deliveryTime != null) {
            goToDeliveryTimeView.text = masterCartDto.carts[0].deliveryTime
        }

        creditCardNumber = view.findViewById(R.id.credit_card_number_input_text)
        creditCardExpirationMonth = view.findViewById(R.id.credit_card_month_expiration_input_text)
        creditCardExpirationYear = view.findViewById(R.id.credit_card_year_expiration_input_text)
        creditCardCVV = view.findViewById(R.id.credit_card_cvv_input_text)

        setUpBottomCard(view)

        view.setOnClickListener {
            val inputMethodManager = tabBarActivity.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
            inputMethodManager.hideSoftInputFromWindow(view.windowToken, 0)
        }

        return view
    }

    private fun setUpBottomCard(customerDataView: View) {
        val bottomCardView = customerDataView.findViewById<View>(R.id.customer_data_card_bottom_sheet_view)

        val bottomSheetBehavior = BottomSheetBehavior.from(bottomCardView)

        bottomSheetBehavior.state = BottomSheetBehavior.STATE_EXPANDED

        val selectedScheduleDayDto = shoppingCartController.getSelectedTimeDelivery()
        if (selectedScheduleDayDto?.amount != null && selectedScheduleDayDto.amount > 0) {
            val priceFeeTextView = bottomCardView.findViewById<TextView>(R.id.customer_data_delivery_fee_price)
            val feeAmount = (selectedScheduleDayDto.amount / 100).toString() + "€"
            priceFeeTextView.setTextColor(Color.RED)
            priceFeeTextView.text = feeAmount
        }

        val storesNameTextView = bottomCardView.findViewById<TextView>(R.id.customer_data_stores_names)
        var storeNames = shoppingCart.stores[0].name

        val storeImageView1 = bottomCardView.findViewById<ImageView>(R.id.customer_data_store_one_image)

        firebaseStorage.reference.child("stores/" + shoppingCart.stores[0].name + ".png")
                .getBytes(1 * 1024 * 1024).addOnSuccessListener {image ->
                    val options = BitmapFactory.Options()
                    val bitmap = BitmapFactory.decodeByteArray(image, 0, image.size, options)
                    storeImageView1.setImageBitmap(bitmap)
                }

        if (shoppingCart.stores.size > 1) {
            val storeImageView2 = bottomCardView.findViewById<ImageView>(R.id.customer_data_store_two_image)

            storeNames = storeNames + " + " + shoppingCart.stores[1].name

            firebaseStorage.reference.child("stores/" + shoppingCart.stores[1].name + ".png")
                    .getBytes(1 * 1024 * 1024).addOnSuccessListener { image ->
                        val options = BitmapFactory.Options()
                        val bitmap = BitmapFactory.decodeByteArray(image, 0, image.size, options)
                        storeImageView2.setImageBitmap(bitmap)
                    }
        }

        storesNameTextView.text = storeNames

        val priceTextView = bottomCardView.findViewById<TextView>(R.id.customer_data_master_cart_price)
        val price = shoppingCart.totalAmount.toString() + "€"
        priceTextView.text = price

        val confirmPayButton = bottomCardView.findViewById<Button>(R.id.customer_data_payment_button)
        confirmPayButton.setOnClickListener {
            if (validateFields()) {
                tabBarActivity.sendMetricFirebaseAnalytics(FirebaseMetricsName.purchaseShoppingCart())
                val email = tabBarActivity.userLoginRegisterController.getUser()?.email
                val slackMessage = "Order made by: $email"
                tabBarActivity.shauttSlackMessageService.sendSlackMessage(SlackChannel.ORDER, slackMessage)
                shoppingCartController.confirmAndPayMasterCart(PaymentDataDto(masterCartDto.id.toInt(),
                        creditCardNumber.editText?.text.toString(), creditCardExpirationMonth.editText?.text.toString().toInt(),
                        creditCardExpirationYear.editText?.text.toString().toInt(),
                        creditCardCVV.editText?.text.toString()))
            }
        }
    }

    private fun validateFields() : Boolean {
        if (!Objects.isNull(masterCartDto.deliveryAddress) || !Objects.isNull(masterCartDto.carts[0].deliveryTime)) {
            val regexCreditCardAndCVV = "([0-9]*)".toRegex()
            if (!regexCreditCardAndCVV.matches(creditCardNumber.editText?.text.toString())) {
                displayCustomerDataAlert("Error con la targeta de credito proporcionada. Intente añadir tu tarjeta de crédito sin espacios ni guiones.")
                return false
            }
            if (!regexCreditCardAndCVV.matches(creditCardCVV.editText?.text.toString())) {
                displayCustomerDataAlert("Indica el código de seguridad correctamente.")
                return false
            }
            val regexMonthAndYear = "([0-9]){2}".toRegex()
            if (!regexMonthAndYear.matches(creditCardExpirationMonth.editText?.text.toString())) {
                displayCustomerDataAlert("Indica el mes de la fecha de caducidad de la tarjeta correctamente. Ejemplo 01.")
                return false
            }
            if (!regexMonthAndYear.matches(creditCardExpirationYear.editText?.text.toString())) {
                displayCustomerDataAlert("Indica el año de la fecha de caducidad de la tarjeta correctamente. Ejemplo 21.")
                return false
            }
            return masterCartDto.deliveryAddress?.address != "" && masterCartDto.carts[0].deliveryTime != ""
                    && creditCardNumber.editText?.text.toString() != ""
                    && creditCardCVV.editText?.text.toString() != ""
                    && creditCardExpirationMonth.editText?.text.toString() != ""
                    && creditCardExpirationYear.editText?.text.toString() != ""
        }

        displayCustomerDataAlert("Todos los campos son necesarios.")

        return false
    }

    private fun displayCustomerDataAlert(message: String) {
        val dialogBuilder = AlertDialog.Builder(tabBarActivity)
        val customerAlertPopupView = layoutInflater.inflate(R.layout.customer_data_alert_popup, null)

        dialogBuilder.setView(customerAlertPopupView)
        val dialog = dialogBuilder.create()

        val messageText = customerAlertPopupView.findViewById<TextView>(R.id.customer_data_alert_popup_description)
        messageText.text = message
        val acceptButton = customerAlertPopupView.findViewById<Button>(R.id.customer_data_popup_accept_btn)
        acceptButton.setOnClickListener {
            dialog.dismiss()
        }

        dialog.show()
    }

    fun setShoppingCartMasterCart(shoppingCart: ShoppingCartCandidateDto, masterCartDto: MasterCartDto) {
        this.shoppingCart = shoppingCart
        this.masterCartDto = masterCartDto
    }

    override fun showPreviousMasterCart(
        shoppingCart: ShoppingCartCandidateDto,
        masterCartDto: MasterCartDto
    ) {
        tabBarActivity.goToCustomerDataView(shoppingCart, masterCartDto)
    }

    override fun updateAddressCustomerData(shoppingCart: ShoppingCartCandidateDto, masterCartDto: MasterCartDto) {
        tabBarActivity.goToCustomerDataView(shoppingCart, masterCartDto)
    }

    override fun updateDeliveryTimeCustomerData(shoppingCart: ShoppingCartCandidateDto, masterCartDto: MasterCartDto) {
        tabBarActivity.goToCustomerDataView(shoppingCart, masterCartDto)
    }

    override fun showConfirmationOrder() {
        tabBarActivity.goToConfirmationPage()
    }

    override fun displayConfirmationError() {
        displayCustomerDataAlert("Disculpa, ha habido un error. Inténtalo de nuevo")
    }
}