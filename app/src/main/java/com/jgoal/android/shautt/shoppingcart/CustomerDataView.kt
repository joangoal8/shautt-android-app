package com.jgoal.android.shautt.shoppingcart

import com.jgoal.android.shautt.model.MasterCartDto
import com.jgoal.android.shautt.model.ShoppingCartCandidateDto

interface CustomerDataView {

    fun showPreviousMasterCart(shoppingCart: ShoppingCartCandidateDto, masterCartDto: MasterCartDto)

    fun updateAddressCustomerData(shoppingCart: ShoppingCartCandidateDto, masterCartDto: MasterCartDto)

    fun updateDeliveryTimeCustomerData(shoppingCart: ShoppingCartCandidateDto, masterCartDto: MasterCartDto)

    fun showConfirmationOrder()

    fun displayConfirmationError()
}