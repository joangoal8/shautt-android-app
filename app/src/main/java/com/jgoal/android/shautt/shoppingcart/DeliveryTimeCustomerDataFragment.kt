package com.jgoal.android.shautt.shoppingcart

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.Menu
import android.view.MenuInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ListView
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.fragment.app.Fragment
import com.google.firebase.storage.FirebaseStorage
import com.jgoal.android.shautt.R
import com.jgoal.android.shautt.TabBarActivity
import com.jgoal.android.shautt.adapters.DeliveryTimeDaysAdapter
import com.jgoal.android.shautt.adapters.DeliveryTimeHoursAdapter
import com.jgoal.android.shautt.commons.CustomSpinnerDialog
import com.jgoal.android.shautt.commons.FirebaseMetricsName
import com.jgoal.android.shautt.model.MasterCartDto
import com.jgoal.android.shautt.model.ScheduleDeliveryTimeDto

class DeliveryTimeCustomerDataFragment(private val shoppingCartController: ShoppingCartController,
                                       private val masterCartDto: MasterCartDto): Fragment(), DeliveryTimeView {

    private val deliveryTimeCustomerDataViewTitle = "Horario de entrega"

    private lateinit var tabBarActivity : TabBarActivity

    private var customSpinnerDialog: CustomSpinnerDialog? = null

    private lateinit var daysListView : ListView
    private lateinit var hoursListView: ListView

    private var selectedDay = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.products_items_menu, menu)
        return super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onCreateView(
            inflater: LayoutInflater, container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_delivery_time_customer_data, container, false)

        tabBarActivity = (requireActivity() as AppCompatActivity) as TabBarActivity

        val toolbar : Toolbar = view.findViewById(R.id.delivery_time_customer_data_custom_nav_bar)
        toolbar.title = deliveryTimeCustomerDataViewTitle

        val menu: Menu = toolbar.menu

        val goBackToCustomerDataView = menu.findItem(R.id.icon_back_delivery_time_to_customer_data_view)
        goBackToCustomerDataView.setOnMenuItemClickListener {
            shoppingCartController.showCustomerMasterCart()
            true
        }

        daysListView = view.findViewById(R.id.delivery_time_list_days)
        hoursListView = view.findViewById(R.id.delivery_time_list_hours)

        setSchedulesToListViews(shoppingCartController.getDeliveryTime())

        return view
    }

    private fun setSchedulesToListViews(schedules: List<ScheduleDeliveryTimeDto>) {
        daysListView.adapter = DeliveryTimeDaysAdapter(tabBarActivity, selectedDay, schedules)

        daysListView.setOnItemClickListener { _, _, rowPos, _ ->
            selectedDay = rowPos
            tabBarActivity.sendMetricFirebaseAnalytics(FirebaseMetricsName.getShoppingCartDayTime())
            daysListView.adapter = DeliveryTimeDaysAdapter(tabBarActivity, selectedDay, schedules)
            hoursListView.adapter = DeliveryTimeHoursAdapter(tabBarActivity, schedules[rowPos].deliveryHours)
        }

        if (schedules.isNotEmpty()) {
            hoursListView.adapter = DeliveryTimeHoursAdapter(tabBarActivity, schedules[selectedDay].deliveryHours)
        } else if (customSpinnerDialog == null) {
            customSpinnerDialog = CustomSpinnerDialog(tabBarActivity)
            customSpinnerDialog?.startAlertDialog()
        }

        hoursListView.setOnItemClickListener { _, _, rowPos, _ ->
            tabBarActivity.sendMetricFirebaseAnalytics(FirebaseMetricsName.addShoppingCartDeliveryTime())
            shoppingCartController.setSelectedTimeDelivery(schedules[selectedDay].deliveryHours[rowPos])
        }
    }

    override fun displayDeliveryTimeSchedule(schedules: List<ScheduleDeliveryTimeDto>) {
        customSpinnerDialog?.stopAlertDialog()
        tabBarActivity.runOnUiThread {
            setSchedulesToListViews(schedules)
        }
    }

    override fun displayDeliveryTimeError() {
        Log.d("EXCEPTION ", " In delivery time")
    }
}