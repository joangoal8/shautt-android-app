package com.jgoal.android.shautt.shoppingcart

import com.jgoal.android.shautt.model.ScheduleDeliveryTimeDto

interface DeliveryTimeView {

    fun displayDeliveryTimeSchedule(schedules: List<ScheduleDeliveryTimeDto>)

    fun displayDeliveryTimeError()
}