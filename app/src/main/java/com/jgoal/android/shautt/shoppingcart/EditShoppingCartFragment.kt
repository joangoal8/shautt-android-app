package com.jgoal.android.shautt.shoppingcart

import android.os.Bundle
import android.view.LayoutInflater
import android.view.Menu
import android.view.MenuInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ListView
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.fragment.app.Fragment
import com.jgoal.android.shautt.R
import com.jgoal.android.shautt.TabBarActivity
import com.jgoal.android.shautt.adapters.EditShoppingCartProductsAdapter
import com.jgoal.android.shautt.adapters.ShoppingCartProductsAdapter
import com.jgoal.android.shautt.model.ShoppingCartCandidateDto
import com.jgoal.android.shautt.model.ShoppingCartProductDto

class EditShoppingCartFragment: Fragment(), EditShoppingCartView {

    private val editShoppingCartViewTitle = "Elige tus productos"

    private lateinit var shoppingCart: ShoppingCartCandidateDto

    private lateinit var editableShoppingCartProducts : ListView
    private lateinit var shoppingCartController: ShoppingCartController
    private lateinit var tabBarActivity : TabBarActivity

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.products_items_menu, menu)
        return super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onCreateView(
            inflater: LayoutInflater, container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_edit_shopping_cart_products, container, false)

        editableShoppingCartProducts = view.findViewById(R.id.edit_shopping_cart_products)
        tabBarActivity = (requireActivity() as AppCompatActivity) as TabBarActivity
        shoppingCartController = tabBarActivity.shoppingCartController

        val toolbar : Toolbar = view.findViewById(R.id.edit_shopping_cart_products_nav_bar)
        toolbar.title = editShoppingCartViewTitle

        val menu: Menu = toolbar.menu

        val goBackToProviderShoppingCarts = menu?.findItem(R.id.icon_back_view_in_shopping_cart)
        goBackToProviderShoppingCarts.setOnMenuItemClickListener {
            tabBarActivity.updateShoppingCartView()
            true
        }

        val updateShoppingCartBtn = menu?.findItem(R.id.icon_edit_entire_shopping_cart)
        updateShoppingCartBtn.setOnMenuItemClickListener {
            shoppingCartController.updateShoppingCart(shoppingCart)
            tabBarActivity.updateShoppingCartView()
            true
        }
        editableShoppingCartProducts.adapter = EditShoppingCartProductsAdapter(tabBarActivity, this, shoppingCart.shoppingCartProducts)

        return view
    }

    override fun setShoppingCartEditable(shoppingCart: ShoppingCartCandidateDto) {
        this.shoppingCart = shoppingCart
    }

    override fun editTemporalShoppingCart(shoppingCartProduct: ShoppingCartProductDto, shoppingCartProductIndex: Int) {
        shoppingCart.shoppingCartProducts[shoppingCartProductIndex].selectedProduct = shoppingCartProduct
    }

    override fun getTemporalShoppingCart() : ShoppingCartCandidateDto {
        return shoppingCart
    }

}