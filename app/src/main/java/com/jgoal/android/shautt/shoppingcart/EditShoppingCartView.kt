package com.jgoal.android.shautt.shoppingcart

import com.jgoal.android.shautt.model.ShoppingCartCandidateDto
import com.jgoal.android.shautt.model.ShoppingCartProductDto

interface EditShoppingCartView {

    fun setShoppingCartEditable(shoppingCart : ShoppingCartCandidateDto)

    fun editTemporalShoppingCart(shoppingCartProduct: ShoppingCartProductDto, shoppingCartProductIndex: Int)

    fun getTemporalShoppingCart() : ShoppingCartCandidateDto
}