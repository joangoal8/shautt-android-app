package com.jgoal.android.shautt.shoppingcart

import android.os.Bundle
import android.view.LayoutInflater
import android.view.Menu
import android.view.MenuInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ListView
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.fragment.app.Fragment
import com.jgoal.android.shautt.R
import com.jgoal.android.shautt.TabBarActivity
import com.jgoal.android.shautt.adapters.ProviderShoppingCartCandidatesAdapter
import com.jgoal.android.shautt.commons.CustomSpinnerDialog
import com.jgoal.android.shautt.commons.FirebaseMetricsName
import com.jgoal.android.shautt.model.ShoppingCartCandidateDto
import java.util.*

class ProviderShoppingCartsFragment : Fragment(), ProviderShoppingCartsView {

    private val providersShoppingCartsViewTitle = "Resultados"

    private lateinit var providersShoppingCartListView : ListView
    private lateinit var shoppingCartController: ShoppingCartController
    private lateinit var tabBarActivity : TabBarActivity
    private var customSpinnerDialog: CustomSpinnerDialog? = null

    private var shoppingCarts = Collections.emptyList<ShoppingCartCandidateDto>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.products_items_menu, menu)
        return super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_providers_shopping_carts, container, false)
        providersShoppingCartListView = view.findViewById(R.id.candidates_shopping_carts)
        tabBarActivity = (requireActivity() as AppCompatActivity) as TabBarActivity
        shoppingCartController = tabBarActivity.shoppingCartController
        shoppingCartController.setProviderShoppingCartsView(this)
        val toolbar : Toolbar = view.findViewById(R.id.providers_shopping_cart_custom_nav_bar)
        toolbar.title = providersShoppingCartsViewTitle

        val menu: Menu = toolbar.menu

        val goBackToShoppingList = menu?.findItem(R.id.icon_shopping_carts_back_view)
        goBackToShoppingList.setOnMenuItemClickListener {
            tabBarActivity.returnToShoppingListFragment()
            true
        }

        if (customSpinnerDialog == null) {
            customSpinnerDialog = CustomSpinnerDialog(tabBarActivity)
            customSpinnerDialog?.startAlertDialog()
        }

        return view
    }

    override fun showShoppingCartCandidates(shoppingCarts: List<ShoppingCartCandidateDto>) {
        this.shoppingCarts = shoppingCarts
        customSpinnerDialog?.stopAlertDialog()
        tabBarActivity.runOnUiThread {
            tabBarActivity.shoppingCarts = shoppingCarts
            providersShoppingCartListView.adapter = ProviderShoppingCartCandidatesAdapter(tabBarActivity, tabBarActivity.firebaseStorage, shoppingCarts)
            providersShoppingCartListView.setOnItemClickListener { _, _, rowPos, _ ->
                tabBarActivity.sendMetricFirebaseAnalytics(FirebaseMetricsName.choseStoreInShoppingList())
                tabBarActivity.goToShoppingCartView(shoppingCarts[rowPos])
            }
        }
    }

    override fun displayShoppingCartsError(title: String, text: String) {
        tabBarActivity.runOnUiThread {
            customSpinnerDialog?.stopAlertDialog()
            tabBarActivity.showCustomAlert(title, text)
            tabBarActivity.returnToShoppingListFragment()
        }
    }
}