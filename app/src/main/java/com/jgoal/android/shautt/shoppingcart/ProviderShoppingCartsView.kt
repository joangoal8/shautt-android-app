package com.jgoal.android.shautt.shoppingcart

import com.jgoal.android.shautt.model.ShoppingCartCandidateDto

interface ProviderShoppingCartsView {

    fun showShoppingCartCandidates(shoppingCarts : List<ShoppingCartCandidateDto>)

    fun displayShoppingCartsError(title: String, text: String)
}