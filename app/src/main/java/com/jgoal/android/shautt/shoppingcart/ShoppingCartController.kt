package com.jgoal.android.shautt.shoppingcart

import com.jgoal.android.shautt.model.MasterCartDto
import com.jgoal.android.shautt.model.OrderDto
import com.jgoal.android.shautt.model.PaymentDataDto
import com.jgoal.android.shautt.model.ProductTypeDto
import com.jgoal.android.shautt.model.ScheduleDayDto
import com.jgoal.android.shautt.model.ScheduleDeliveryTimeDto
import com.jgoal.android.shautt.model.ShoppingCartCandidateDto
import com.jgoal.android.shautt.model.UserAddressDto

interface ShoppingCartController {

    fun searchShoppingCarts(postcode: String, shoppingList: List<ProductTypeDto>)

    fun setProviderShoppingCartsView(providerShoppingCartsView: ProviderShoppingCartsView)

    fun setShoppingCartViewWithShoppingCart(shoppingCartView: ShoppingCartView, shoppingCart: ShoppingCartCandidateDto)

    fun updateShoppingCart(shoppingCart : ShoppingCartCandidateDto)

    fun getCurrentShoppingCart() : ShoppingCartCandidateDto

    fun confirmShoppingCart(postcode: String, shoppingCart : ShoppingCartCandidateDto)

    fun setToken(token: String?)

    fun setCustomerDataView(customerDataView: CustomerDataView)

    fun showCustomerMasterCart()

    fun registerMasterCart(userAddressDto: UserAddressDto, addressMore: String, phone: String)

    fun retrieveDeliveryTime(cartIds: List<String>, providerName: String)

    fun getDeliveryTime() : List<ScheduleDeliveryTimeDto>

    fun setDeliveryTimeView(deliveryTimeView: DeliveryTimeView)

    fun setSelectedTimeDelivery(scheduleDayDto: ScheduleDayDto)

    fun getSelectedTimeDelivery() : ScheduleDayDto?

    fun confirmAndPayMasterCart(paymentDataDto: PaymentDataDto)

    fun getMasterCartDto(): MasterCartDto

    fun getOrderDto() : OrderDto
}