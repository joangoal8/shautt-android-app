package com.jgoal.android.shautt.shoppingcart

import android.util.Log
import com.jgoal.android.shautt.model.*
import com.jgoal.android.shautt.repository.ShoppingCartRepository
import com.jgoal.android.shautt.user.UserViewModel
import kotlinx.serialization.json.Json
import okhttp3.Call
import okhttp3.Callback
import okhttp3.Response
import org.json.JSONObject
import java.io.IOException
import java.util.*
import javax.inject.Inject
import kotlin.collections.ArrayList

class ShoppingCartControllerImpl @Inject constructor(private val shoppingCartRepository: ShoppingCartRepository,
                                                     private val userViewModel: UserViewModel) : ShoppingCartController {

    private val json = Json { ignoreUnknownKeys = true }

    private lateinit var providerShoppingCartsView: ProviderShoppingCartsView
    private lateinit var shoppingCartView: ShoppingCartView
    private lateinit var customerDataView: CustomerDataView
    private var deliveryTimeView: DeliveryTimeView? = null

    private lateinit var postcode: String
    private lateinit var shoppingCart: ShoppingCartCandidateDto
    private lateinit var masterCart: MasterCartDto
    private lateinit var orderDto: OrderDto

    private var schedules: List<ScheduleDeliveryTimeDto> = Collections.emptyList()
    private var token: String? = null
    private var scheduleDay: ScheduleDayDto? = null

    override fun searchShoppingCarts(postcode: String, shoppingList: List<ProductTypeDto>) {
        this.postcode = postcode
        shoppingCartRepository.searchShoppingCarts(postcode, shoppingList).enqueue(object : Callback {
            override fun onResponse(call: Call, response: Response) {
                if (response.isSuccessful) {
                    if (response.code() == 200) {
                        response.body()?.string()?.let {
                            JSONObject(it)
                                    .getJSONArray("results")
                                    .let { jsonArray ->
                                        val shoppingCartsCandidates = ArrayList<ShoppingCartCandidateDto>()
                                        for (i in 0 until jsonArray.length()) {
                                            val shoppingCartCandidateDtoJson = jsonArray.getJSONObject(i)
                                            shoppingCartsCandidates.add(json.decodeFromString(ShoppingCartCandidateDto.serializer(), shoppingCartCandidateDtoJson.toString()))
                                        }
                                        providerShoppingCartsView.showShoppingCartCandidates(shoppingCartsCandidates)
                                    }
                        }
                    } else {
                        providerShoppingCartsView.displayShoppingCartsError("No disponible", "Lo sentimos pero esta zona aún no está diponsible. Estamos trabajando en ello.")
                    }
                } else {
                    providerShoppingCartsView.displayShoppingCartsError("Error", "Disculpa, ha habido un error. Inténtelo más tarde")
                }
            }

            override fun onFailure(call: Call, e: IOException) {
                e.printStackTrace()
                Log.d("EXCEPTION", e.localizedMessage!!)
                providerShoppingCartsView.displayShoppingCartsError("Error", "Disculpa, hay un problema. Inténtelo más tarde")
            }
        })
    }

    override fun setProviderShoppingCartsView(providerShoppingCartsView: ProviderShoppingCartsView) {
        this.providerShoppingCartsView = providerShoppingCartsView
    }

    override fun setShoppingCartViewWithShoppingCart(shoppingCartView: ShoppingCartView, shoppingCart: ShoppingCartCandidateDto) {
        this.shoppingCartView = shoppingCartView
        this.shoppingCart = shoppingCart
    }

    override fun updateShoppingCart(shoppingCart : ShoppingCartCandidateDto) {
        this.shoppingCart = shoppingCart
    }

    override fun getCurrentShoppingCart(): ShoppingCartCandidateDto {
        return shoppingCart
    }

    override fun confirmShoppingCart(postcode: String, shoppingCart : ShoppingCartCandidateDto) {

        val masterCartItems = shoppingCart.shoppingCartProducts.map { shoppingCartProductTypeDto ->  
            MasterCartItemDto(shoppingCartProductTypeDto.selectedProduct.id,
            shoppingCartProductTypeDto.selectedProduct.productTypeCalculated,
            shoppingCartProductTypeDto.selectedProduct.providerItemId,
            shoppingCartProductTypeDto.selectedProduct.providerName,
            shoppingCartProductTypeDto.quantity)
        }

        val storeIds = shoppingCart.stores.map { storeDto ->
            storeDto.providerId
        }

        shoppingCartRepository.createMasterCart(postcode, storeIds, masterCartItems, shoppingCart.provider).enqueue(object : Callback {
            override fun onResponse(call: Call, response: Response) {
                if (response.isSuccessful) {
                    response.body()?.string()?.let {
                        JSONObject(it)
                                .getJSONObject("result").let { jsonObject ->
                                    masterCart = json.decodeFromString(MasterCartDto.serializer(), jsonObject.toString())
                                    shoppingCartView.showCustomerDataFragment(shoppingCart, masterCart)
                                }
                    }
                } else {
                    shoppingCartView.displayConfirmShoppingCartError()
                }
            }

            override fun onFailure(call: Call, e: IOException) {
                e.printStackTrace()
                Log.d("EXCEPTION", e.localizedMessage!!)
                shoppingCartView.displayConfirmShoppingCartError()
            }
        })
    }

    override fun setToken(token: String?) {
        this.token = token
    }

    override fun setCustomerDataView(customerDataView: CustomerDataView) {
        this.customerDataView = customerDataView
    }

    override fun showCustomerMasterCart() {
        customerDataView.showPreviousMasterCart(shoppingCart, masterCart)
    }

    override fun registerMasterCart(userAddressDto: UserAddressDto, addressMore: String, phone: String) {
        val addressMasterCartDto = AddressMasterCartDto(userAddressDto, postcode, addressMore, phone)
        shoppingCartRepository.registerMasterCart(masterCart, addressMasterCartDto, token).enqueue(object : Callback {
            override fun onResponse(call: Call, response: Response) {
                if (response.isSuccessful) {
                    response.body()?.string()?.let {
                        JSONObject(it)
                            .getJSONObject("result").let { jsonObject ->
                                masterCart = json.decodeFromString(MasterCartDto.serializer(), jsonObject.toString())
                                token = masterCart.token
                                customerDataView.updateAddressCustomerData(shoppingCart, masterCart)
                            }
                    }
                } else {
                    shoppingCartView.displayConfirmShoppingCartError()
                }
            }

            override fun onFailure(call: Call, e: IOException) {
                e.printStackTrace()
                Log.d("EXCEPTION", e.localizedMessage!!)
                shoppingCartView.displayConfirmShoppingCartError()
            }
        })
    }

    override fun retrieveDeliveryTime(cartIds: List<String>, providerName: String) {
        shoppingCartRepository.retrieveDeliveryTime(token!!, cartIds, providerName).enqueue(object : Callback {
            override fun onResponse(call: Call, response: Response) {
                if (response.isSuccessful) {
                    response.body()?.string()?.let {
                        JSONObject(it)
                                .getJSONArray("schedules")
                                .let { jsonArray ->
                                    val schedules = ArrayList<ScheduleDeliveryTimeDto>()
                                    for (i in 0 until jsonArray.length()) {
                                        val scheduleDeliveryTimeDto = jsonArray.getJSONObject(i)
                                        schedules.add(json.decodeFromString(ScheduleDeliveryTimeDto.serializer(), scheduleDeliveryTimeDto.toString()))
                                    }
                                    this@ShoppingCartControllerImpl.schedules = schedules
                                    if (deliveryTimeView != null) {
                                        deliveryTimeView?.displayDeliveryTimeSchedule(schedules)
                                    }
                                }
                    }
                } else {
                    if (deliveryTimeView != null) {
                        deliveryTimeView?.displayDeliveryTimeError()
                    }
                }
            }

            override fun onFailure(call: Call, e: IOException) {
                e.printStackTrace()
                Log.d("EXCEPTION", e.localizedMessage!!)
                if (deliveryTimeView != null) {
                    deliveryTimeView?.displayDeliveryTimeError()
                }
            }
        })
    }

    override fun getDeliveryTime() : List<ScheduleDeliveryTimeDto> {
        if (schedules.isEmpty()) {
            val cartsId = masterCart.carts.map {
                it.id
            }
            this.retrieveDeliveryTime(cartsId, masterCart.providerName)
        }
        return schedules
    }

    override fun setDeliveryTimeView(deliveryTimeView: DeliveryTimeView) {
        this.deliveryTimeView = deliveryTimeView
    }

    override fun setSelectedTimeDelivery(scheduleDayDto: ScheduleDayDto) {
        scheduleDay = scheduleDayDto
        masterCart.carts[0].deliveryTime = scheduleDayDto.time
        customerDataView.updateDeliveryTimeCustomerData(shoppingCart, masterCart)
    }

    override fun getSelectedTimeDelivery() : ScheduleDayDto? {
        return scheduleDay
    }

    override fun confirmAndPayMasterCart(paymentDataDto: PaymentDataDto) {
        shoppingCartRepository
                .confirmPayment(token!!, masterCart, scheduleDay?.time!!, paymentDataDto, userViewModel.getUserDto()?.id).enqueue(object : Callback {
                    override fun onResponse(call: Call, response: Response) {
                        if (response.isSuccessful) {
                            response.body()?.string()?.let {
                                JSONObject(it)
                                        .getJSONObject("result").let { jsonObject ->
                                            orderDto = json.decodeFromString(OrderDto.serializer(), jsonObject.toString())
                                            customerDataView.showConfirmationOrder()
                                        }
                            }
                        } else {
                            customerDataView.displayConfirmationError()
                        }
                    }

                    override fun onFailure(call: Call, e: IOException) {
                        e.printStackTrace()
                        Log.d("EXCEPTION", e.localizedMessage!!)
                        customerDataView.displayConfirmationError()
                    }
                })
    }

    override fun getMasterCartDto(): MasterCartDto {
        return masterCart
    }

    override fun getOrderDto() : OrderDto {
        return orderDto
    }
}