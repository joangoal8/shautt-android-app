package com.jgoal.android.shautt.shoppingcart

import android.graphics.BitmapFactory
import android.os.Bundle
import android.view.LayoutInflater
import android.view.Menu
import android.view.MenuInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.ListView
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.fragment.app.Fragment
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.firebase.storage.FirebaseStorage
import com.jgoal.android.shautt.R
import com.jgoal.android.shautt.TabBarActivity
import com.jgoal.android.shautt.adapters.ShoppingCartProductsAdapter
import com.jgoal.android.shautt.commons.FirebaseMetricsName
import com.jgoal.android.shautt.model.MasterCartDto
import com.jgoal.android.shautt.model.ShoppingCartCandidateDto
import com.jgoal.android.shautt.model.SlackChannel
import kotlin.math.round
import kotlin.math.roundToInt

class ShoppingCartFragment(private val shoppingCartController: ShoppingCartController,
                           private val firebaseStorage: FirebaseStorage): Fragment(), ShoppingCartView {

    private val shoppingCartViewTitle = "Carrito de la compra"

    private lateinit var shoppingCart: ShoppingCartCandidateDto

    private lateinit var shoppingCartListView : ListView
    private lateinit var tabBarActivity : TabBarActivity

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.products_items_menu, menu)
        return super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_shopping_cart, container, false)
        shoppingCartListView = view.findViewById(R.id.selected_shopping_cart_list)
        tabBarActivity = (requireActivity() as AppCompatActivity) as TabBarActivity

        val toolbar : Toolbar = view.findViewById(R.id.shopping_cart_custom_nav_bar)
        toolbar.title = shoppingCartViewTitle

        val menu: Menu = toolbar.menu

        val goBackToProviderShoppingCarts = menu.findItem(R.id.icon_back_view_in_shopping_cart)
        goBackToProviderShoppingCarts.setOnMenuItemClickListener {
            tabBarActivity.goToProviderShoppingCartResults()
            true
        }
        val editShoppingCartBtn = menu?.findItem(R.id.icon_edit_entire_shopping_cart)
        editShoppingCartBtn.setOnMenuItemClickListener {
            tabBarActivity.sendMetricFirebaseAnalytics(FirebaseMetricsName.editAllShoppingCartProduct())
            tabBarActivity.editShoppingCartView(shoppingCart)
            true
        }

        shoppingCartListView.adapter = ShoppingCartProductsAdapter(tabBarActivity, shoppingCart.shoppingCartProducts)
        val listHeight = shoppingCart.shoppingCartProducts.size * (tabBarActivity.resources.displayMetrics.density * 1000)
        shoppingCartListView.layoutParams.height = listHeight.roundToInt()

        shoppingCart.totalAmount = round(getTotalAmountShoppingCart() * 100) / 100
        setUpBottomCard(view)

        shoppingCartListView.setOnItemClickListener { _, _, rowPos, _ ->
            tabBarActivity.sendMetricFirebaseAnalytics(FirebaseMetricsName.editSingleShoppingCartProduct())
            tabBarActivity.goToEditShoppingCartProductView(shoppingCart, rowPos)
        }

        return view
    }

    private fun getTotalAmountShoppingCart() : Double {
        return shoppingCart.shoppingCartProducts.fold(0.0) {
            amount, nextProd -> amount + (nextProd.quantity + nextProd.selectedProduct.price)
        }
    }

    private fun setUpBottomCard(shoppingCartView: View) {
        val bottomCardView = shoppingCartView.findViewById<View>(R.id.card_bottom_sheet_view)

        val bottomSheetBehavior = BottomSheetBehavior.from(bottomCardView)

        bottomSheetBehavior.state = BottomSheetBehavior.STATE_EXPANDED

        val storeImageView1 = bottomCardView.findViewById<ImageView>(R.id.card_view_shopping_cart_store_one_image)

        firebaseStorage.reference.child("stores/" + shoppingCart.stores[0].name + ".png")
                .getBytes(1 * 1024 * 1024).addOnSuccessListener {image ->
                    val options = BitmapFactory.Options()
                    val bitmap = BitmapFactory.decodeByteArray(image, 0, image.size, options)
                    storeImageView1.setImageBitmap(bitmap)
                }

        if (shoppingCart.stores.size > 1) {
            val storeImageView2 = bottomCardView.findViewById<ImageView>(R.id.card_view_shopping_cart_store_two_image)

            firebaseStorage.reference.child("stores/" + shoppingCart.stores[1].name + ".png")
                    .getBytes(1 * 1024 * 1024).addOnSuccessListener { image ->
                        val options = BitmapFactory.Options()
                        val bitmap = BitmapFactory.decodeByteArray(image, 0, image.size, options)
                        storeImageView2.setImageBitmap(bitmap)
                    }
        }

        val priceTextView = bottomCardView.findViewById<TextView>(R.id.card_view_shopping_cart_price)
        val price = shoppingCart.totalAmount.toString() + "€"
        priceTextView.text = price

        val confirmButton = bottomCardView.findViewById<Button>(R.id.card_view_confirm_shopping_cart_button)
        confirmButton.setOnClickListener {
            tabBarActivity.sendMetricFirebaseAnalytics(FirebaseMetricsName.confirmShoppingCart())
            val productsText = shoppingCart.shoppingCartProducts.fold("products \n") {
                    productsTxt, nextProduct -> productsTxt + nextProduct.selectedProduct.name + ", "
            }
            val email = tabBarActivity.userLoginRegisterController.getUser()?.email
            val slackMessage = "Shopping cart products: \n $productsText \n user: $email \n in ANDROID"
            tabBarActivity.shauttSlackMessageService.sendSlackMessage(SlackChannel.CONFIRM, slackMessage)
            shoppingCartController.confirmShoppingCart(tabBarActivity.getPostcode(), shoppingCart)
        }
    }

    override fun setShoppingCart(shoppingCart: ShoppingCartCandidateDto) {
        this.shoppingCart = shoppingCart
    }

    override fun showCustomerDataFragment(shoppingCart: ShoppingCartCandidateDto, masterCart : MasterCartDto) {
        tabBarActivity.goToCustomerDataView(shoppingCart, masterCart)
    }

    override fun displayConfirmShoppingCartError() {

    }
}