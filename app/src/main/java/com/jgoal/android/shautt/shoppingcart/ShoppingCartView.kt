package com.jgoal.android.shautt.shoppingcart

import com.jgoal.android.shautt.model.MasterCartDto
import com.jgoal.android.shautt.model.ShoppingCartCandidateDto

interface ShoppingCartView {

    fun setShoppingCart(shoppingCart : ShoppingCartCandidateDto)

    fun showCustomerDataFragment(shoppingCart: ShoppingCartCandidateDto, masterCart : MasterCartDto)

    fun displayConfirmShoppingCartError()
}