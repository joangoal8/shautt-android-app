package com.jgoal.android.shautt.shoppinglist

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.*
import android.widget.ExpandableListView
import android.widget.SearchView
import androidx.appcompat.widget.Toolbar
import androidx.fragment.app.Fragment
import com.jgoal.android.shautt.R
import com.jgoal.android.shautt.TabBarActivity
import com.jgoal.android.shautt.adapters.CategoryProductsTypeAdapter
import com.jgoal.android.shautt.commons.FirebaseMetricsName
import com.jgoal.android.shautt.model.CategoryProductTypesDTO
import java.util.*
import kotlin.collections.ArrayList

class ProductsFragment : Fragment(), ProductsView {

    private val productsListViewTitle = "Productos"

    private lateinit var shoppingListController: ShoppingListController
    private lateinit var productsTypeListView : ExpandableListView
    private lateinit var tabBarActivity : TabBarActivity

    private var categoryProductTypes: List<CategoryProductTypesDTO> = Collections.emptyList()
    private var filteredCategoryProductTypes: List<CategoryProductTypesDTO> = Collections.emptyList()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.products_items_menu, menu)
        return super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_products, container, false)
        productsTypeListView = view.findViewById(R.id.all_products_type_list)
        tabBarActivity = (requireActivity() as AppCompatActivity) as TabBarActivity

        shoppingListController = tabBarActivity.shoppingListController
        shoppingListController.setProductsView(this)
        val toolbar : Toolbar = view.findViewById(R.id.products_nav_bar)
        toolbar.title = productsListViewTitle

        val menu: Menu = toolbar.menu

        val search = menu?.findItem(R.id.icon_product_search)
        val searchView = search?.actionView as SearchView
        searchView.queryHint = "Busca tu producto"

        val goBackToTabBar = menu?.findItem(R.id.icon_product_back_view)
        searchView.setOnQueryTextListener(object: SearchView.OnQueryTextListener {

            override fun onQueryTextSubmit(text: String?): Boolean {
                if (text == null || text.isEmpty()) {
                    return false
                }
                filterCategoriesProducts(text)
                updateCategoryProducts(filteredCategoryProductTypes)
                return true
            }

            override fun onQueryTextChange(text: String?): Boolean {
                if (text == null || text.isEmpty()) {
                    updateCategoryProducts(categoryProductTypes)
                    return false
                } else {
                    filterCategoriesProducts(text)
                    updateCategoryProducts(filteredCategoryProductTypes)
                }
                return true
            }
        })

        goBackToTabBar.setOnMenuItemClickListener {
            tabBarActivity.returnToShoppingListFragment()
            true
        }
        return view
    }

    override fun onStart() {
        super.onStart()
        shoppingListController.retrieveProductTypes()
    }

    override fun showAllProductTypes(categoryProductTypes: List<CategoryProductTypesDTO>) {
        this.categoryProductTypes = categoryProductTypes
        updateCategoryProducts(categoryProductTypes)
    }

    private fun updateCategoryProducts(categoryProductTypes: List<CategoryProductTypesDTO>) {
        tabBarActivity.runOnUiThread {
            productsTypeListView.setAdapter(
                CategoryProductsTypeAdapter(
                    tabBarActivity,
                    tabBarActivity.firebaseStorage,
                    categoryProductTypes
                )
            )
        }
    }

    override fun displayError() {
        Log.d("ERROR", "ProductsFragment")
    }

    private fun filterCategoriesProducts(name: String) {
        var newFiltered = ArrayList<CategoryProductTypesDTO>()
        categoryProductTypes.forEach { categoryProductTypeDTO ->
            val filterProd = categoryProductTypeDTO.items.filter { productType -> Boolean
                productType.name.toUpperCase().contains(name.toUpperCase())
            }
            if (!filterProd.isEmpty()) {
                newFiltered.add(CategoryProductTypesDTO(categoryProductTypeDTO.name, filterProd, categoryProductTypeDTO.isExpanded))
            }
        }

        filteredCategoryProductTypes = newFiltered
    }

}

