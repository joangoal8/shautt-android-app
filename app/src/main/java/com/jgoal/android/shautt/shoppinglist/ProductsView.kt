package com.jgoal.android.shautt.shoppinglist

import com.jgoal.android.shautt.model.CategoryProductTypesDTO

interface ProductsView {

    fun showAllProductTypes(categoryProductTypes : List<CategoryProductTypesDTO>)

    fun displayError()
}