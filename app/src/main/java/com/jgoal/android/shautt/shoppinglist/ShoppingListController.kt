package com.jgoal.android.shautt.shoppinglist

import com.jgoal.android.shautt.model.ProductTypeDto

interface ShoppingListController {

    fun setShoppingListView(shoppingListView: ShoppingListView)

    fun setProductsView(productsView: ProductsView)

    fun retrieveShoppingList()

    fun retrieveProductTypes()

    fun updateShoppingListProductType(productTypeDto: ProductTypeDto)

    fun getAndShareShoppingList()
}