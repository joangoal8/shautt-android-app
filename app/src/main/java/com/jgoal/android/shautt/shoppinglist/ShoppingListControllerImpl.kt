package com.jgoal.android.shautt.shoppinglist

import android.util.Log
import com.jgoal.android.shautt.model.CategoryProductTypesDTO
import com.jgoal.android.shautt.model.ProductTypeDto
import com.jgoal.android.shautt.repository.ShoppingListRepository
import com.jgoal.android.shautt.user.UserViewModel
import kotlinx.serialization.json.Json
import okhttp3.Call
import okhttp3.Callback
import okhttp3.Response
import org.json.JSONObject
import java.io.IOException
import javax.inject.Inject

class ShoppingListControllerImpl @Inject constructor(private val shoppingListRepository: ShoppingListRepository,
                                                     private val userViewModel: UserViewModel) : ShoppingListController {

    private lateinit var shoppingListView: ShoppingListView
    private lateinit var productsView: ProductsView
    private val json = Json { ignoreUnknownKeys = true }

    override fun setShoppingListView(shoppingListView: ShoppingListView) {
        this.shoppingListView = shoppingListView
    }

    override fun setProductsView(productsView: ProductsView) {
        this.productsView = productsView
    }

    override fun retrieveShoppingList() {
        shoppingListRepository.retrieveShoppingList(userViewModel.getUserDto()?.shoppingListId!!)
            .addOnSuccessListener { list ->
                shoppingListView.showShoppingList(list)
            }
            .addOnFailureListener {
                shoppingListView.displayError()
            }
    }

    override fun retrieveProductTypes() {
        shoppingListRepository.retrieveAllProductTypes().enqueue(object : Callback {
            override fun onResponse(call: Call, response: Response) {
                if (response.isSuccessful) {
                    response.body()?.string()?.let {
                        JSONObject(it)
                            .getJSONArray("results")
                            .let { jsonArray ->
                                val categoryProductTypesDto = ArrayList<CategoryProductTypesDTO>()
                                for (i in 0 until jsonArray.length()) {
                                    val catProdJson = jsonArray.getJSONObject(i)
                                    categoryProductTypesDto.add(json.decodeFromString(CategoryProductTypesDTO.serializer(), catProdJson.toString()))
                                }
                                productsView.showAllProductTypes(categoryProductTypesDto)
                            }
                    }
                } else {
                    productsView.displayError()
                }
            }

            override fun onFailure(call: Call, e: IOException) {
                e.printStackTrace()
                Log.d("EXCEPTION", e.localizedMessage!!)
                productsView.displayError()
            }

        })
    }

    override fun updateShoppingListProductType(productTypeDto: ProductTypeDto) {
        if (userViewModel.getUserDto() == null) {
            return
        }
        shoppingListRepository.editShoppingList(userViewModel.getUserDto()?.shoppingListId!!, productTypeDto)
            .addOnSuccessListener { list ->
            shoppingListView.showShoppingList(list)
            }
            .addOnFailureListener {
                Log.d("ERROR", " Error updating shoppingList")
            }
    }

    override fun getAndShareShoppingList() {
        shoppingListRepository.retrieveShoppingList(userViewModel.getUserDto()?.shoppingListId!!)
                .addOnSuccessListener { list ->
                    shoppingListView.shareShoppingList(list)
                }
                .addOnFailureListener {
                    shoppingListView.displayError()
                }
    }

}