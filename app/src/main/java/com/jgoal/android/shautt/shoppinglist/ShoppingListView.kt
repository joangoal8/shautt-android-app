package com.jgoal.android.shautt.shoppinglist

import com.jgoal.android.shautt.model.ProductTypeDto

interface ShoppingListView {

    fun showShoppingList(shoppingList : List<ProductTypeDto>)

    fun displayError()

    fun shareShoppingList(shoppingList : List<ProductTypeDto>)
}