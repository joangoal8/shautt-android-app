package com.jgoal.android.shautt.user

import com.jgoal.android.shautt.model.UserDto

class UserViewModel {

    private var userDto: UserDto? = null

    fun getUserDto() : UserDto? {
        return userDto
    }

    fun setUserDto(userDto: UserDto?) {
        this.userDto = userDto
    }

}