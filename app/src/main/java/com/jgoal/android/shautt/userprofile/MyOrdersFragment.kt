package com.jgoal.android.shautt.userprofile

import android.os.Bundle
import android.view.LayoutInflater
import android.view.Menu
import android.view.MenuInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ListView
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.fragment.app.Fragment
import com.jgoal.android.shautt.R
import com.jgoal.android.shautt.TabBarActivity

class MyOrdersFragment(private val userProfileController: UserProfileController): Fragment(), UserProfileView {

    private val myOrdersViewTitle = "Tus pedidos"

    private lateinit var ordersListView : ListView

    private lateinit var tabBarActivity : TabBarActivity

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.products_items_menu, menu)
        return super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onCreateView(
            inflater: LayoutInflater, container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_my_orders, container, false)

        tabBarActivity = (requireActivity() as AppCompatActivity) as TabBarActivity

        val toolbar : Toolbar = view.findViewById(R.id.my_orders_custom_nav_bar)
        toolbar.title = myOrdersViewTitle

        val menu: Menu = toolbar.menu

        val backToUserProfile = menu.findItem(R.id.icon_back_to_user_profile)
        backToUserProfile.setOnMenuItemClickListener {
            tabBarActivity.returnToUserProfileFragment()
            true
        }

        ordersListView = view.findViewById(R.id.customer_my_orders_list)

        return view
    }

    override fun displayMyOrders() {
        TODO("Not yet implemented")
    }

}