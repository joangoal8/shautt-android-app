package com.jgoal.android.shautt.userprofile

interface UserProfileController {

    fun retrieveUserOrders()
}