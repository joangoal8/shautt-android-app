package com.jgoal.android.shautt.userprofile

import okhttp3.Call

interface UserProfileRepository {

    fun retrieveUserOrders(userId: String) : Call
}